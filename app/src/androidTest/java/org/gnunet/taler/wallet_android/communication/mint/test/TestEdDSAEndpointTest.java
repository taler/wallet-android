/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.communication.CommunicationTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertTrue;

/**
 * Runs a test against the mint's /test/eddsa endpoint
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class TestEdDSAEndpointTest {

    private TestEdDSAEndpoint endpoint;

    @After
    public void tearDown() {
        endpoint.close();
    }

    @Test
    public void testIt() throws IOException {
        try (EdDSAPrivateKey privateKey = new EdDSAPrivateKey();
             EdDSAPublicKey publicKey = privateKey.getPublicKey();
             ECCSignaturePurpose signaturePurpose =
                     new ECCSignaturePurpose(null, 0,
                             TalerLibrary.Taler_Signatures.TALER_SIGNATURE_CLIENT_TEST_EDDSA);
             EdDSASignature signature = new EdDSASignature(signaturePurpose, privateKey)) {

            endpoint = new TestEdDSAEndpoint(
                    new URL(CommunicationTestingConstants.TEST_MINT),
                    publicKey,
                    signature);
            endpoint.connect();
        }

        assertTrue("The client could not verify the received signature!",
                endpoint.getReceivedSignature().verify(
                        endpoint.getReceivedSignaturePurpose(),
                        endpoint.getReceivedPublicKey()));
    }
}
