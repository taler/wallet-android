/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.utilities;

import android.support.test.runner.AndroidJUnit4;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.PointerUtils;

import org.gnunet.taler.wallet_android.nativeLibs.CryptoTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the Codec helper class
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class CodecTest implements CryptoTestingConstants {
    @Test
    public void testIntegrity() {
        Memory decodedString =
                Codec.decode(prebuiltMessageHashBase32);
        String reencodedString = Codec.encode(
                decodedString,
                new NativeSize(decodedString.size()));

        assertEquals(prebuiltMessageHashBase32, reencodedString);
    }

    @Test
    public void testIntegrityReverse() {
        long decodedData = Long.reverseBytes(473148305465370L);
        ByteBuffer buffer = ByteBuffer.allocateDirect(8).putLong(decodedData);
        Pointer bufferPointer = Native.getDirectBufferPointer(buffer);

        String encodedData = Codec.encode(
                bufferPointer,
                new NativeSize(buffer.capacity()));

        Memory redecoded = Codec.decode(encodedData);
        ByteBuffer redecodedBuffer = Native.getDirectByteBuffer(
                PointerUtils.getAddress(redecoded),
                redecoded.size());

        long redecodedData = redecodedBuffer.asLongBuffer().get();
        assertEquals(decodedData, redecodedData);
    }
}
