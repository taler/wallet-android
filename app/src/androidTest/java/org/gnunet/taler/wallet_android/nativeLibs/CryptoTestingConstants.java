/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs;

/**
 * Constants used for testing the crypto functions
 *
 * @author Oliver Broome
 */
public interface CryptoTestingConstants {
    String prebuiltMessage = "This is an ex-parrot.";
    String prebuiltMessageHashBase16 =
            "507f7443aeea7363ea70794a7ea0f3ae30331b03703c3adb9131683abf311a8240d452b3691ca3aa12b2" +
                    "9d573a5e08f82adc6984d021c8291e494405f2d58491";
    String prebuiltMessageHashBase32 =
            "A1ZQ8GXEX9SP7TKGF557X87KNRR366R3E0Y3NPWH65M3NFSH3A141N2JPDMHS8XA2AS9TNSTBR4FGAPWD62D" +
                    "08E854F4JH05YBAR948";
    String prebuiltMessageSignature =
            "(sig-val \n" +
                    " (rsa \n" +
                    "  (s #93DB4BB07C2D265D3DCF704AF2931758AE2F942D5A6191624233B1E20339C8F0F81CB9" +
                    "B5F36687097BECD8DB759C569FD1D9617DBAB3FB16B2E1ADA27D7405E2C9CDFC3969FBB61549" +
                    "74E998D9622CE511AA6D8B09E9253068875149828CE0E31F789D0FC6D6587B62A13DB1B012AD" +
                    "FAADBC6AD1B45B58BB4A03DC99EEE07B3E#)\n" +
                    "  )\n" +
                    " )\n";
    String prebuiltKey =
            "(key-data \n" +
                    " (public-key \n" +
                    "  (rsa \n" +
                    "   (n #00BD3F3030B67670710CC3F2BE86568E92110B2E8820090EC95EC077D12D87FC987C0" +
                    "B4F8E3549CDFED68AD6A0AF46299BB8FDDAED6B3A429EA021A6CC4B9032EDD20C5A3" +
                    "EDE696BBECA8D6D1DD506C1DA0DA0AD4DC412F8B766BE863647E5F3FE6BD8B4421D8" +
                    "549261DB544F62A5F0B8DD0F84BE347CBF15D4916AF6FA1D9A047#)\n" +
                    "   (e #010001#)\n" +
                    "   )\n" +
                    "  )\n" +
                    " (private-key \n" +
                    "  (rsa \n" +
                    "   (n #00BD3F3030B67670710CC3F2BE86568E92110B2E8820090EC95EC077D12D87FC987C0" +
                    "B4F8E3549CDFED68AD6A0AF46299BB8FDDAED6B3A429EA021A6CC4B9032EDD20C5A3" +
                    "EDE696BBECA8D6D1DD506C1DA0DA0AD4DC412F8B766BE863647E5F3FE6BD8B4421D8" +
                    "549261DB544F62A5F0B8DD0F84BE347CBF15D4916AF6FA1D9A047#)\n" +
                    "   (e #010001#)\n" +
                    "   (d #0517A3B0D3E0190379BFF612B963AFDF6F54F93211869CDEBF4D778BE9E93C5B500F2" +
                    "FDB5844BDA5CB591E1061D705FC9A3C3E7D6002C7D323E4F3EB9066F76FD1E27A6F5" +
                    "9D540F92287BBB8810A68853545AB1D12BD2AECE857E9E4B9E44ED908211B4068693" +
                    "F9CCED06CEA37C4FD82ACD308576A0410344FE6286F51F87821#)\n" +
                    "   (p #00C1463441E7DDB878A08AF710D039A182CBC11838CE4F2640DC21F6B434B0D706860" +
                    "FEE400B107C738D7040C63A8CA80AC0090A0EE4731315E1D559A16C120CE7#)\n" +
                    "   (q #00FAAA60144BBE2C6E5FE2A225962001D6D0CE9D523DCE084F046E823483D9EA09D43" +
                    "2FAC6185011B73831502928EA152BC5DA8C0462D7A3813CA744069EE905A1#)\n" +
                    "   (u #1F1DD1E95EE17CAFD848DA159D1614104D93249F0C357F34F97F67B97925BBFBABEDC" +
                    "BEB21EF08E489D672C9F94B7FAB556BA2D4C4463505823EC30E387CDE78#)\n" +
                    "   )\n" +
                    "  )\n" +
                    " )\n";
}
