/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.ecc;

import android.support.test.runner.AndroidJUnit4;

import com.sun.jna.Native;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.ByteBuffer;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for ECC signature purposes
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class ECCSignaturePurposeTest {
    private String testString = "Look, mate, this parrot wouldn't 'voo' if I put four thousand volts through it!";
    private ByteBuffer testStringBuffer;

    private int testPurpose = GNUNetLibrary.GNUNET_SIGNATURE_Purpose.GNUNET_SIGNATURE_PURPOSE_TEST;

    private ECCSignaturePurpose signaturePurpose;

    private ByteBuffer prepareBuffer() {
        byte[] stringBytes = Native.toByteArray(testString);
        ByteBuffer stringBuffer = ByteBuffer.allocateDirect(stringBytes.length);
        stringBuffer.put(stringBytes);

        return stringBuffer;
    }

    @Before
    public void setUp() {
        testStringBuffer = prepareBuffer();
    }

    @After
    public void tearDown() {
        if (signaturePurpose != null) {
            signaturePurpose.close();
            signaturePurpose = null;
        }

        testStringBuffer = null;
    }

    @Test
    public void testInitialisationViaByteBuffer() {
        signaturePurpose = new ECCSignaturePurpose(testStringBuffer, testPurpose);

        assertNotNull("No data Pointer returned!", signaturePurpose.getDataPointer());
        assertNotNull("No signature purpose structure returned!", signaturePurpose.getStruct());

        assertNotEquals("Purpose size is 0!", 0, signaturePurpose.getSize());
        assertTrue("Purpose size is too small!", signaturePurpose.getSize() > 8);
        assertTrue("Purpose size is too large!", (testStringBuffer.capacity() + 8) == signaturePurpose.getSize());
    }

    @Test
    public void testInitialisationViaPointer() {
        signaturePurpose = new ECCSignaturePurpose(
                Native.getDirectBufferPointer(testStringBuffer),
                testStringBuffer.capacity(),
                testPurpose);

        assertNotNull("No data Pointer returned!", signaturePurpose.getDataPointer());
        assertNotNull("No signature purpose structure returned!", signaturePurpose.getStruct());

        assertNotEquals("Purpose size is 0!", 0, signaturePurpose.getSize());
        assertTrue("Purpose size is too small!", signaturePurpose.getSize() > 8);
        assertTrue("Purpose size is too large!", (testStringBuffer.capacity() + 8) == signaturePurpose.getSize());
    }

    @Test
    public void testDataIntegrity() {
        signaturePurpose = new ECCSignaturePurpose(
                Native.getDirectBufferPointer(testStringBuffer),
                testStringBuffer.capacity(),
                testPurpose);

        String retrievedString = signaturePurpose.getDataPointer().getString(0);

        assertTrue(testString.equals(retrievedString));
    }
}
