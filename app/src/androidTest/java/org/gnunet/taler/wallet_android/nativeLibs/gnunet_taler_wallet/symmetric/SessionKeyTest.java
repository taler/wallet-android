/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.symmetric;

import android.support.test.runner.AndroidJUnit4;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.SessionKey;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for symmetric key generation
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class SessionKeyTest {
    private final String testPassphrase = "The pet shop man's brother was lying!";

    @Test
    public void testGeneration() {
        try (SessionKey sessionKey = new SessionKey()) {
            assertNotNull("The session key was not returned!", sessionKey);
            assertTrue("The session key is null!", sessionKey.getData().getPointer().getByte(0) != 0);
        }
    }

    @Test
    public void testDerivation() {
        byte[] passphraseBytes = testPassphrase.getBytes(StandardCharsets.US_ASCII);
        Memory passphraseMemory = new Memory(passphraseBytes.length);

        passphraseMemory.write(0, passphraseBytes, 0, passphraseBytes.length - 1);

        try (SessionKey result = new SessionKey(passphraseMemory, new NativeSize(passphraseMemory.size()), new String[]{"walletpw"})) {
            assertNotNull("No Memory was returned by the KDF!", result);
        }
    }
}
