/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.communication.CommunicationTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EcDHEPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EcDHEPublicKey;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Runs a test against the mint's /test/ecdhe endpoint
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class TestEcDHEEndpointTest {

    private TestEcDHEEndpoint endpoint;

    @After
    public void tearDown() {
        endpoint.close();
    }

    @Test
    public void testIt() throws IOException {
        try (EcDHEPrivateKey privateKey = new EcDHEPrivateKey();
             EcDHEPublicKey publicKey = privateKey.getPublicKey()) {

            endpoint = new TestEcDHEEndpoint(
                    new URL(CommunicationTestingConstants.TEST_MINT),
                    privateKey,
                    publicKey);
            endpoint.connect();
        }

        assertNotNull("The expected response was not created!", endpoint.getExpectedResult());
        assertNotNull("The actual response was not parsed!", endpoint.getActualResult());

        assertEquals("The local and remotely-generated values don't match!",
                endpoint.getExpectedResult(),
                endpoint.getActualResult());
    }
}
