/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.communication.CommunicationTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindedMessage;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertTrue;


/**
 * Runs a test against both the /test/rsa/get and /test/rsa/sign endpoints
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class TestRSASignEndpointTest {
    private TestRSAGetEndpoint getEndpoint;
    private TestRSASignEndpoint signEndpoint;

    @After
    public void tearDown() {
        getEndpoint.close();
        signEndpoint.close();
    }

    @Test
    public void testIt() throws IOException {
        try (Hash hashedMessage = new Hash("I'll tell you what's wrong with it - it's dead, that's what's wrong with it.")) {

            URL testURL = new URL(CommunicationTestingConstants.TEST_MINT);

            getEndpoint = new TestRSAGetEndpoint(testURL);
            getEndpoint.connect();

            try (RSAPublicKey mintPubKey = getEndpoint.getReceivedPublicKey();

                 RSABlindingKey blindingKey = new RSABlindingKey(2048);
                 RSABlindedMessage blindedMessage = new RSABlindedMessage(
                         hashedMessage,
                         blindingKey,
                         mintPubKey)) {

                signEndpoint = new TestRSASignEndpoint(testURL, blindedMessage);
                signEndpoint.connect();

                try (RSASignature unblindedResponse = new RSASignature(
                        signEndpoint.getReceivedSignature(),
                        blindingKey,
                        mintPubKey)) {

                    assertTrue("Could not verify the signature!", unblindedResponse.verify(hashedMessage, mintPubKey));
                }
            }
        }
    }
}
