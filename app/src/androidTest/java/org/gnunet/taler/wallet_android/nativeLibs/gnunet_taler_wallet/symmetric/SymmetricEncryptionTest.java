/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.symmetric;

import android.support.test.runner.AndroidJUnit4;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.InitialisationVector;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.SessionKey;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;

/**
 * Integration tests for symmetric encryption
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class SymmetricEncryptionTest {

    @Test
    public void testEncryptionWithGeneratedKey() {
        try (SessionKey key = new SessionKey();
             InitialisationVector iv = new InitialisationVector(key, new String[]{""})) {

            String secret = "Well, you can't blame British Rail for that!";
            Memory secretMemory = new Memory(secret.length() + 1);
            secretMemory.setString(0, secret, "ASCII");

            Memory encryptedData = key.encrypt(secretMemory, new NativeSize(secretMemory.size()), iv);

            Memory decryptedData = key.decrypt(encryptedData, new NativeSize(secretMemory.size()), iv);

            String decryptedSecret = decryptedData.getString(0, "ASCII");

            assertEquals("Decryption yielded a different string!", secret, decryptedSecret);
        }
    }
}
