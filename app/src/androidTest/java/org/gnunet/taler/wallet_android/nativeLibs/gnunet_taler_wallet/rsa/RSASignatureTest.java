/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.rsa;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.nativeLibs.CryptoTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindedMessage;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for RSA signatures
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class RSASignatureTest implements CryptoTestingConstants {
    private RSAPrivateKey privateKey;
    private RSASignature signature;
    private Hash hashedMessage;

    @After
    public void tearDown() {
        if (signature != null) {
            signature.close();
            signature = null;
        }
        if (privateKey != null) {
            privateKey.close();
            privateKey = null;
        }
        if (hashedMessage != null) {
            hashedMessage.close();
            hashedMessage = null;
        }
    }

    @Before
    public void setUp() {
        privateKey = new RSAPrivateKey(CryptoTestingConstants.prebuiltKey);
        hashedMessage = new Hash(prebuiltMessage);
    }

    @Test
    public void testInitialisation() {
        signature = new RSASignature(hashedMessage, privateKey);

        assertNotNull("Signature structure is empty!", signature.getStruct());
        assertEquals("Signature does not equal prebuilt version!",
                signature.encode(),
                prebuiltMessageSignature);
    }

    @Test
    public void testInitWithStringEncodedSignature() {
        signature = new RSASignature(prebuiltMessageSignature, false);

        assertNotNull("Signature structure is empty!", signature.getStruct());
        assertEquals("Signature does not equal prebuilt version!",
                signature.encode(),
                prebuiltMessageSignature);
    }

    @Test
    public void testInitWithStruct() {
        try (RSASignature secondSignature = new RSASignature(prebuiltMessageSignature, false)) {
            signature = new RSASignature(secondSignature);

            assertNotNull("Signature structure is empty!", signature.getStruct());
            assertEquals("Signatures aren't equal!", secondSignature.encode(), signature.encode());
        }
    }

    @Test
    public void testInitWithHash() {
        signature = new RSASignature(hashedMessage, privateKey);

        assertNotNull(signature.getStruct());
        assertEquals("The signature doesn't have the expected value!",
                prebuiltMessageSignature,
                signature.encode());
    }


    @Test
    public void testInitWithBlindedSignature() {
        try (RSABlindingKey blindingKey = new RSABlindingKey(2048);
             RSASignature originalSignature = new RSASignature(hashedMessage, privateKey);
             RSABlindedMessage blindedMessage = new RSABlindedMessage(hashedMessage, blindingKey, privateKey.getPublicKey());
             RSASignature blindedSignature = new RSASignature(blindedMessage, privateKey)) {
            signature = new RSASignature(blindedSignature, blindingKey, privateKey.getPublicKey());

            assertNotNull(signature.getStruct());
            assertTrue(signature.encode().length() > 1);
            assertEquals(originalSignature.encode(), signature.encode());
        }
    }

    @Test
    public void testInitWithBlindedMessage() {
        try (RSABlindingKey blindingKey = new RSABlindingKey(2048);
             RSABlindedMessage blindedMessage = new RSABlindedMessage(
                     hashedMessage,
                     blindingKey,
                     privateKey.getPublicKey())) {

            signature = new RSASignature(blindedMessage, privateKey);

            assertNotNull(signature.getStruct());
        }
    }
}
