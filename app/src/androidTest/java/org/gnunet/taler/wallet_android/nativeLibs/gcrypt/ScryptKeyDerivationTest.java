/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gcrypt;

import android.support.test.runner.AndroidJUnit4;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.NativeLong;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.RandomBlock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for the libgcrypt KDF wrapper
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class ScryptKeyDerivationTest {
    private String passphrase = "Well, he's...he's, ah...probably pining for the fjords.";
    private RandomBlock salt;

    @Before
    public void setUp() {
        salt = new RandomBlock(RandomBlock.SALT_BLOCK);
    }

    @Test
    public void testDerivation() {
        Memory result = ScryptKeyDerivation.derive(passphrase,
                salt.getPointer(),
                salt.getNativeSize(),
                new NativeLong(200),
                new NativeSize(64));

        assertNotNull("ScryptKeyDerivation.derive did not return a result!", result);
        assertTrue("The derived key is null!", result.getByte(0) != 0);
    }

    @Test
    public void testValues() {
        Memory result = ScryptKeyDerivation.derive(passphrase,
                salt.getPointer(),
                salt.getNativeSize(),
                new NativeLong(50),
                new NativeSize(64));

        salt = new RandomBlock(RandomBlock.SALT_BLOCK);
        Memory otherResult = ScryptKeyDerivation.derive(passphrase,
                salt.getPointer(),
                salt.getNativeSize(),
                new NativeLong(50),
                new NativeSize(64));

        assertFalse("Different salts gave the same result!",
                Arrays.equals(
                        result.getByteArray(0, (int) result.size()),
                        otherResult.getByteArray(0, (int) otherResult.size())));
    }
}
