/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.objects;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for the amount descriptor
 *
 * @author Oliver Broome
 */
public class AmountDescriptorTest {
    @Test
    public void testNormalise() {
        AmountDescriptor amount = new AmountDescriptor("EUR", 1, 10000001);

        assertEquals(
                "The fraction was not reduced to 1!",
                amount.getFraction(),
                1);

        assertEquals(
                "The value was not increased to 11!",
                amount.getValue(),
                11
        );
    }

    @Test
    public void testStringConstructor() {
        AmountDescriptor pointAmount = new AmountDescriptor("EUR", "100.01");
        AmountDescriptor commaAmount = new AmountDescriptor("EUR", "100,01");

        assertEquals("The values of the items isn't identical!",
                pointAmount.getValue(),
                commaAmount.getValue());

        assertEquals("The fractions of the items isn't identical!",
                pointAmount.getFraction(),
                commaAmount.getFraction());
    }

    @Test(expected = NumberFormatException.class)
    public void testMultipleSeparators() {
        AmountDescriptor invalidAmount = new AmountDescriptor("EUR", "100.,10.10");
    }
}