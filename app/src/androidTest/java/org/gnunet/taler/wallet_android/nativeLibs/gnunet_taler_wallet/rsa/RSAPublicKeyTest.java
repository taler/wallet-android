/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunet_taler_wallet.rsa;

import android.support.test.runner.AndroidJUnit4;

import org.gnunet.taler.wallet_android.nativeLibs.CryptoTestingConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPublicKey;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNotNull;

/**
 * Unit tests for the RSAPublicKey class
 *
 * @author Oliver Broome
 */
@RunWith(AndroidJUnit4.class)
public class RSAPublicKeyTest implements CryptoTestingConstants {
    @Test
    public void testEncoding() {
        try (RSAPrivateKey newPrivateKey = new RSAPrivateKey(1024);
             RSAPublicKey newPublicKey = newPrivateKey.getPublicKey()) {

            String newEncodedKey = newPublicKey.encode();

            assertNotNull("Encoded string is null!", newEncodedKey);
        }
    }

    @Test
    public void testDecoding() {
        try (RSAPublicKey newPublicKey = new RSAPublicKey(prebuiltKey, false)) {
            assertNotNull("The private key object is null!", newPublicKey);
            assertNotNull("The private key's structure is null!", newPublicKey.getStruct());
        }
    }
}
