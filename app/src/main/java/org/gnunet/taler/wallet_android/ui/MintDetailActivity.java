/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.gnunet.taler.wallet_android.R;
import org.gnunet.taler.wallet_android.utilities.StableArrayAdapter;

import java.util.ArrayList;
import java.util.Collections;

public class MintDetailActivity extends AppCompatActivity {
    private long mintId;
    private String mintName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mintId = getIntent().getLongExtra(IntentConstants.EXTRA_MINT_ID, -1);
        mintName = getIntent().getStringExtra(IntentConstants.EXTRA_MINT_NAME);

        setContentView(R.layout.activity_mint_detail);

        ListView reserveListView = (ListView) findViewById(R.id.reserveList);

        String[] values = new String[]{"Reserve 1", "Reserve 2", "Reserve 3", "Reserve 4"};
        final ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, values);

        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        reserveListView.setAdapter(adapter);
        reserveListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), ReserveDetailActivity.class);

                intent.putExtra(IntentConstants.EXTRA_RESERVE_ID, id);
                intent.putExtra(IntentConstants.EXTRA_MINT_NAME, mintName);
                intent.putExtra(IntentConstants.EXTRA_MINT_ID, mintId);

                startActivity(intent);
            }
        });
    }

    public void launchReserveGenerator(View view) {
        Intent intent = new Intent(this, ReserveGeneratorActivity.class);

        intent.putExtra(IntentConstants.EXTRA_MINT_ID, mintId);
        startActivity(intent);
    }
}
