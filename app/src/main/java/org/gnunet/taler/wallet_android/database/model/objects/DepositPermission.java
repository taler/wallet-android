/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.objects;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.Contract;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.customStructs.TALER_DepositPermission;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TALER_Amount;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Represents a Taler deposit permission
 *
 * @author Oliver Broome
 */
public class DepositPermission implements AutoCloseable {
    private TALER_DepositPermission depositStruct;
    private TALER_Amount hostAmount;
    private Hash descriptionHash;

    public DepositPermission(Contract contract, CoinDescriptor coin, EdDSAPublicKey merchantPublicKey) {
        hostAmount = new TALER_Amount.ByReference();
        TalerLibrary.TALER_amount_ntoh(hostAmount, contract.getAmountStruct());

        depositStruct = new TALER_DepositPermission(
                coin.getCoinPublicInfoStruct(),
                contract.getContractID(),
                hostAmount,
                new Hash(contract.getDescription()),
                contract.getWireInfoStruct(),
                merchantPublicKey.getStruct());
    }

    public TALER_DepositPermission getDepositStruct() {
        return depositStruct;
    }

    public int getStructSize() {
        return depositStruct.size();
    }

    @Override
    public void close() {
        if (depositStruct != null) {
            MemoryUtils.freeStruct(depositStruct);
            depositStruct = null;
        }

        if (hostAmount != null) {
            MemoryUtils.freeStruct(hostAmount);
            hostAmount = null;
        }

        if (descriptionHash != null) {
            descriptionHash.close();
            descriptionHash = null;
        }
    }
}
