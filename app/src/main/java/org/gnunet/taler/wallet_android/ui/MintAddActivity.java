/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.gnunet.taler.wallet_android.R;
import org.gnunet.taler.wallet_android.database.model.infrastructure.MintDescriptor;
import org.gnunet.taler.wallet_android.ui.fragments.MintAddWorkerFragment;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Activity for adding mints to the wallet
 *
 * @author Oliver Broome
 */
public class MintAddActivity extends AppCompatActivity implements MintAddWorkerFragment.Callbacks {
    // Tag so we can find the task fragment again, in another instance of this fragment after rotation.
    static final String TASK_FRAGMENT_TAG = "task";
    private FragmentManager fragmentManager;
    private MintAddWorkerFragment workerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mint_add);

        fragmentManager = getSupportFragmentManager();

        workerFragment = (MintAddWorkerFragment) fragmentManager.findFragmentByTag(TASK_FRAGMENT_TAG);

        if (workerFragment == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mintAddAsyncContainer, new MintAddWorkerFragment(), TASK_FRAGMENT_TAG);
            fragmentTransaction.commit();
            fragmentManager.executePendingTransactions();

            workerFragment = (MintAddWorkerFragment) fragmentManager.findFragmentByTag(TASK_FRAGMENT_TAG);
        }

        Button addButton = (Button) findViewById(R.id.mintAddButton);
        addButton.setOnClickListener(new AddButtonListener());
    }

    public void addMint(String mintName, URL mintURL) throws IOException {
        MintDescriptor mint = new MintDescriptor(-1, mintName, mintURL, null);

        workerFragment.startTask(mint);
    }

    @Override
    public void taskComplete(long mintId) {
        Intent intent = new Intent(this, MintDetailActivity.class);
        intent.putExtra(IntentConstants.EXTRA_MINT_ID, -1);

        startActivity(intent);
    }

    private class AddButtonListener implements View.OnClickListener {
        TextView urlWarning = (TextView) findViewById(R.id.mintValidLabel);

        @Override
        public void onClick(View v) {
            try {
                String mintURLString = ((EditText) findViewById(R.id.mintUrlInput)).getEditableText().toString();
                URL mintURL = new URL(mintURLString);
                String mintName = ((EditText) findViewById(R.id.mintNameInput)).getText().toString();

                addMint(mintName, mintURL);
            } catch (MalformedURLException e) {
                urlWarning.setTextColor(Color.RED);
                urlWarning.setText(R.string.invalid_mint_URL);
                urlWarning.setAlpha(1);
            } catch (IOException e) {
                urlWarning.setTextColor(Color.BLUE);
                urlWarning.setText(R.string.not_a_taler_mint);
                urlWarning.setAlpha(1);
            }
        }
    }
}
