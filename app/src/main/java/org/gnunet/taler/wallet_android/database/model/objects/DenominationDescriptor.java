/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.objects;

import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPublicKey;

import java.util.Calendar;

/**
 * Represents a denomination as used by the mint.
 *
 * @author Oliver Broome
 */
public class DenominationDescriptor implements AutoCloseable {
    private int id;

    private int mintId = -1;

    private RSAPublicKey publicKey;
    private EdDSASignature masterSignature;

    private Calendar start = Calendar.getInstance();
    private Calendar depositExpiry = Calendar.getInstance();
    private Calendar legalExpiry = Calendar.getInstance();
    private Calendar withdrawalExpiry = Calendar.getInstance();

    private AmountDescriptor value;
    private AmountDescriptor withdrawalFee;
    private AmountDescriptor depositFee;
    private AmountDescriptor refreshFee;

    public DenominationDescriptor(@NonNull RSAPublicKey publicKey,
                                  @NonNull EdDSASignature masterSignature,
                                  @NonNull AmountDescriptor value,
                                  @NonNull AmountDescriptor depositFee,
                                  @NonNull AmountDescriptor refreshFee,
                                  @NonNull AmountDescriptor withdrawFee,
                                  long start,
                                  long depositExpiry, long legalExpiry, long withdrawExpiry) {
        this.publicKey = publicKey;
        this.masterSignature = masterSignature;

        this.start.setTimeInMillis(start);
        this.depositExpiry.setTimeInMillis(depositExpiry);
        this.legalExpiry.setTimeInMillis(legalExpiry);
        this.withdrawalExpiry.setTimeInMillis(withdrawExpiry);

        this.depositFee = depositFee;
        this.refreshFee = refreshFee;
        this.withdrawalFee = withdrawFee;

        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMintId() {
        return mintId;
    }

    public void setMintId(int mintId) {
        this.mintId = mintId;
    }

    public RSAPublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(RSAPublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public EdDSASignature getMasterSignature() {
        return masterSignature;
    }

    public void setMasterSignature(EdDSASignature masterSignature) {
        this.masterSignature = masterSignature;
    }

    public AmountDescriptor getValue() {
        return value;
    }

    public void setValue(AmountDescriptor value) {
        this.value = value;
    }

    public AmountDescriptor getWithdrawalFee() {
        return withdrawalFee;
    }

    public void setWithdrawalFee(AmountDescriptor withdrawalFee) {
        this.withdrawalFee = withdrawalFee;
    }

    public AmountDescriptor getDepositFee() {
        return depositFee;
    }

    public void setDepositFee(AmountDescriptor depositFee) {
        this.depositFee = depositFee;
    }

    public AmountDescriptor getRefreshFee() {
        return refreshFee;
    }

    public void setRefreshFee(AmountDescriptor refreshFee) {
        this.refreshFee = refreshFee;
    }

    public Calendar getWithdrawalExpiry() {
        return withdrawalExpiry;
    }

    public void setWithdrawalExpiry(Calendar withdrawalExpiry) {
        this.withdrawalExpiry = withdrawalExpiry;
    }

    public Calendar getDepositExpiry() {
        return depositExpiry;
    }

    public void setDepositExpiry(Calendar depositExpiry) {
        this.depositExpiry = depositExpiry;
    }

    public Calendar getStart() {
        return start;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public Calendar getLegalExpiry() {
        return legalExpiry;
    }

    public void setLegalExpiry(Calendar legalExpiry) {
        this.legalExpiry = legalExpiry;
    }

    @Override
    public void close() {
        if (masterSignature != null) {
            masterSignature.close();
            masterSignature = null;
        }

        if (publicKey != null) {
            publicKey.close();
            publicKey = null;
        }
    }
}
