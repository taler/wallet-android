/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.util.Log;

import org.gnunet.taler.wallet_android.database.DatabaseFactory;
import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.CoinDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.DenominationDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSASignature;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AND;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AUTOINCREMENT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BLOB;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BOOLEAN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.CHECK;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.COMMA;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.DEFAULT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.DOT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.EQUALS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.FALSE;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.FROM;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.IS_PARAMETER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.LIMIT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NOT_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.ON_DELETE;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.PRIMARY_KEY;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.REFERENCES;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.SELECT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.SET_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.TEXT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.WHERE;

/**
 * Allows access to the coins table
 */
public class CoinsDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "coins";
    public static final String VIEW_NAME = "reserve_coins";

    public static final String COLUMN_WITHDRAW_ID = "withdrawid";
    public static final String COLUMN_REFRESH_ID = "refreshid";
    public static final String COLUMN_DENOMINATION_ID = "denomid";
    public static final String COLUMN_COIN_PUBLIC = "coin_pub";
    public static final String COLUMN_X_COIN_PRIVATE = "x_coin_priv";
    public static final String COLUMN_IS_REFRESHED = "is_refreshed";
    public static final String COLUMN_EXPENDED_VALUE = "expended_value";
    public static final String COLUMN_EXPENDED_FRACTION = "expended_fraction";
    public static final String COLUMN_DENOMINATION_SIGNATURE = "denom_sig";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + BEGIN_COLUMNS +
                    BaseColumns._ID + INTEGER + PRIMARY_KEY + AUTOINCREMENT + NEXT_COLUMN +
                    COLUMN_WITHDRAW_ID + INTEGER + REFERENCES + WithdrawDatabase.TABLE_NAME + " (" + BaseColumns._ID + ")" + ON_DELETE + SET_NULL + NEXT_COLUMN +
                    COLUMN_REFRESH_ID + INTEGER + REFERENCES + RefreshDatabase.TABLE_NAME + " (" + BaseColumns._ID + ")" + ON_DELETE + SET_NULL + NEXT_COLUMN +
                    COLUMN_DENOMINATION_ID + INTEGER + REFERENCES + DenominationDatabase.TABLE_NAME + " (" + BaseColumns._ID + ")" + NOT_NULL + NEXT_COLUMN +
                    COLUMN_COIN_PUBLIC + BLOB + NOT_NULL + NEXT_COLUMN +
                    COLUMN_X_COIN_PRIVATE + BLOB + NOT_NULL + NEXT_COLUMN +
                    COLUMN_IS_REFRESHED + BOOLEAN + DEFAULT + FALSE + NEXT_COLUMN +
                    COLUMN_EXPENDED_VALUE + INTEGER + DEFAULT + "0" + NEXT_COLUMN +
                    COLUMN_EXPENDED_FRACTION + INTEGER + DEFAULT + "0" + NEXT_COLUMN +
                    COLUMN_DENOMINATION_SIGNATURE + TEXT + NOT_NULL + NEXT_COLUMN +
                    " -- either coin originates from withdraw or from refresh" + NEXT_COLUMN +
                    " -- or it is imported (both NULL)\n" +
                    CHECK + "((" + COLUMN_WITHDRAW_ID + " ISNULL) + (" + COLUMN_REFRESH_ID + " ISNULL) >= 1)" +
                    END_COLUMNS;

    public static final String VIEW_CREATE =
            "CREATE VIEW IF NOT EXISTS " + VIEW_NAME + AS + "\n" +
                    SELECT +
                    TABLE_NAME + DOT + BaseColumns._ID + AS + BaseColumns._ID + NEXT_COLUMN +
                    ReserveDatabase.TABLE_NAME + DOT + BaseColumns._ID + AS + "reserveid" + NEXT_COLUMN +
                    TABLE_NAME + DOT + COLUMN_WITHDRAW_ID + AS + COLUMN_WITHDRAW_ID + NEXT_COLUMN +
                    TABLE_NAME + DOT + COLUMN_REFRESH_ID + AS + COLUMN_REFRESH_ID + NEXT_COLUMN +
                    TABLE_NAME + DOT + COLUMN_DENOMINATION_ID + AS + COLUMN_DENOMINATION_ID + NEXT_COLUMN +
                    TABLE_NAME + DOT + COLUMN_COIN_PUBLIC + AS + COLUMN_COIN_PUBLIC + NEXT_COLUMN +
                    TABLE_NAME + DOT + COLUMN_X_COIN_PRIVATE + AS + COLUMN_X_COIN_PRIVATE + NEXT_COLUMN +
                    TABLE_NAME + DOT + COLUMN_IS_REFRESHED + AS + COLUMN_IS_REFRESHED + NEXT_COLUMN +
                    TABLE_NAME + DOT + COLUMN_EXPENDED_VALUE + AS + COLUMN_EXPENDED_VALUE + NEXT_COLUMN +
                    TABLE_NAME + DOT + COLUMN_EXPENDED_FRACTION + AS + COLUMN_EXPENDED_FRACTION + NEXT_COLUMN +
                    TABLE_NAME + DOT + COLUMN_DENOMINATION_SIGNATURE + AS + COLUMN_DENOMINATION_SIGNATURE + "\n" +
                    FROM + "\n" +
                    ReserveDatabase.TABLE_NAME + COMMA + CoinsDatabase.TABLE_NAME + COMMA + WithdrawDatabase.TABLE_NAME + "\n" +
                    WHERE + "\n" +
                    ReserveDatabase.TABLE_NAME + DOT + BaseColumns._ID + EQUALS +
                    WithdrawDatabase.TABLE_NAME + DOT + WithdrawDatabase.COLUMN_RESERVE_ID + "\n" +
                    AND + "\n" +
                    CoinsDatabase.TABLE_NAME + DOT + COLUMN_WITHDRAW_ID + EQUALS +
                    WithdrawDatabase.TABLE_NAME + DOT + BaseColumns._ID;

    public static final String WITHDRAW_RESERVE_SELECTOR =
            SELECT + WithdrawDatabase.COLUMN_RESERVE_ID +
                    FROM + WithdrawDatabase.TABLE_NAME +
                    WHERE + CoinsDatabase.COLUMN_WITHDRAW_ID + IS_PARAMETER +
                    AND + CoinsDatabase.TABLE_NAME + DOT + COLUMN_WITHDRAW_ID + EQUALS +
                    WithdrawDatabase.TABLE_NAME + DOT + BaseColumns._ID + LIMIT + "1;";

    public static final String REFRESH_RESERVE_SELECTOR =
            null; //TODO
    private static final String RESERVE_ID_WHERE = "reserveid" + IS_PARAMETER;
    private static final String COIN_ID_WHERE = BaseColumns._ID + IS_PARAMETER;

    public CoinsDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }

    public Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }

    public long getReserveForWithdrawal(long withdrawId) {
        try (SQLiteDatabase database = databaseHelper.getReadableDatabase();
             SQLiteStatement statement = database.compileStatement(WITHDRAW_RESERVE_SELECTOR)) {

            statement.bindLong(1, withdrawId);
            return statement.simpleQueryForLong();
        }
    }

    public long getReserveForRefresh(long refreshId) {
        try (SQLiteDatabase database = databaseHelper.getReadableDatabase();
             SQLiteStatement statement = database.compileStatement(REFRESH_RESERVE_SELECTOR)) {

            statement.bindLong(1, refreshId);
            return statement.simpleQueryForLong();
        }
    }

    public Cursor getCoinsForReserve(long reserveId) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        return database.query(VIEW_NAME,
                null,
                RESERVE_ID_WHERE,
                new String[]{Long.toString(reserveId)},
                null, null, null, null);
    }

    public long insertCoin(CoinDescriptor coin) {
        ContentValues values = new ContentValues(8);

        if (coin.isRefreshed()) {
            values.put(COLUMN_REFRESH_ID, coin.getOriginId());
        } else {
            values.put(COLUMN_WITHDRAW_ID, coin.getOriginId());
        }

        values.put(COLUMN_DENOMINATION_ID, coin.getDenomination().getId());
        values.put(COLUMN_COIN_PUBLIC, coin.getPublicKey().getStruct().q_y);
        values.put(COLUMN_X_COIN_PRIVATE, coin.getPrivateKey().getStruct().d);
        values.put(COLUMN_IS_REFRESHED, coin.isRefreshed());
        values.put(COLUMN_DENOMINATION_SIGNATURE, coin.getSignature().encode());

        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            return database.insert(TABLE_NAME, null, values);
        }
    }

    public boolean spendCoin(long coinId, AmountDescriptor amount) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            ContentValues values = new ContentValues(2);

            values.put(COLUMN_EXPENDED_VALUE, amount.getValue());
            values.put(COLUMN_EXPENDED_FRACTION, amount.getFraction());

            return database.update(TABLE_NAME,
                    values,
                    COIN_ID_WHERE,
                    new String[]{Long.toString(coinId)}) == 1;
        }
    }

    private class Reader extends DatabaseReader {
        /**
         * Instantiates a new Reader for a given Cursor
         *
         * @param cursor the Cursor which this Reader should read from
         */
        public Reader(@NonNull Cursor cursor) {
            super(cursor);
        }

        @Override
        public CoinDescriptor getCurrent() {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(BaseColumns._ID));
            long originId = -1;
            long reserveId = -1;
            boolean refreshed = false;

            try {
                originId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_WITHDRAW_ID));
                reserveId = getReserveForWithdrawal(originId);
            } catch (SQLException ignored) {
            }

            try {
                originId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_REFRESH_ID));
                reserveId = getReserveForRefresh(originId);
                refreshed = true;
            } catch (SQLException ignored) {
            }

            if (originId == -1) {
                Log.wtf("taler_database", "The database isn't checking constraints properly!");
            }

            long denominationId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_DENOMINATION_ID));

            DenominationDescriptor denomination =
                    DatabaseFactory.getDenominationDatabase(null).getDenominationForId(denominationId);

            EdDSAPrivateKey privateKey =
                    new EdDSAPrivateKey(cursor.getBlob(cursor.getColumnIndexOrThrow(COLUMN_X_COIN_PRIVATE)));

            privateKey.setPublicKey(new EdDSAPublicKey(
                    cursor.getBlob(cursor.getColumnIndexOrThrow(COLUMN_COIN_PUBLIC))
            ));

            AmountDescriptor currentValue = null;

            int expendedValue = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_EXPENDED_VALUE));
            int expendedFraction = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_EXPENDED_FRACTION));

            RSASignature signature = new RSASignature(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DENOMINATION_SIGNATURE)), false);

            if (expendedValue > 0 || expendedFraction > 0) {
                AmountDescriptor originalValue = denomination.getValue();
                currentValue = new AmountDescriptor(
                        originalValue.getCurrency(),
                        originalValue.getValue(),
                        originalValue.getFraction());
            }

            return new CoinDescriptor(privateKey, denomination, currentValue, signature, refreshed, originId, id, reserveId);
        }
    }
}
