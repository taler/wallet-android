/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gcrypt.jna;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.ochafik.lang.jnaerator.runtime.NativeSizeByReference;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

import java.nio.ByteBuffer;

/**
 * Vararg functions from libgcrypt
 */
public interface GcryptVarargLibrary {
    GcryptVarargLibrary INSTANCE = (GcryptVarargLibrary)
            Native.loadLibrary(GcryptLibrary.JNA_LIBRARY_NAME,
                    GcryptVarargLibrary.class);

    /**
     * Perform various operations defined by CMD.<br>
     * Original signature : {@code gcry_error_t gcry_control(gcry_ctl_cmds, null)}
     */
    int gcry_control(int CMD, Object... varArgs1);

    /**
     * Same as gcry_sexp_sscan but expects a string in FORMAT and can thus<br>
     * only be used for certain encodings.<br>
     * Original signature : {@code gcry_error_t gcry_sexp_build(gcry_sexp_t*, size_t*, const char*, null)}
     */
    int gcry_sexp_build(PointerByReference retsexp, NativeSizeByReference erroff, String format, Object... varArgs1);

    /**
     * Original signature : {@code gcry_sexp_t gcry_sexp_vlist(const gcry_sexp_t, null)}
     */
    GcryptLibrary.gcry_sexp_t gcry_sexp_vlist(GcryptLibrary.gcry_sexp_t a, Object... varArgs1);

    /**
     * Convenience fucntion to extract parameters from an S-expression<br>
     * using a list of single letter parameters.<br>
     * Original signature : {@code gpg_error_t gcry_sexp_extract_param(gcry_sexp_t, const char*, const char*, null)}
     */
    int gcry_sexp_extract_param(GcryptLibrary.gcry_sexp_t sexp, String path, String list, Object... varArgs1);

    /**
     * Log data using Libgcrypt's own log interface.<br>
     * Original signature : {@code void gcry_log_debug(const char*, null)}
     */
    void gcry_log_debug(String fmt, Object... varArgs1);

    /**
     * Original signature : {@code int gpgrt_fprintf(gpgrt_stream_t, const char*, null)}
     */
    int gpgrt_fprintf(_gpgrt__stream stream, String format, Object... varArgs1);

    /**
     * Original signature : {@code int gpgrt_fprintf_unlocked(gpgrt_stream_t, const char*, null)}
     */
    int gpgrt_fprintf_unlocked(_gpgrt__stream stream, String format, Object... varArgs1);

    /**
     * Original signature : {@code int gpgrt_printf(const char*, null)}
     */
    int gpgrt_printf(String format, Object... varArgs1);

    /**
     * Original signature : {@code int gpgrt_printf_unlocked(const char*, null)}
     */
    int gpgrt_printf_unlocked(String format, Object... varArgs1);

    /**
     * Original signature : {@code int gpgrt_vfprintf(gpgrt_stream_t, const char*, va_list)}
     */
    int gpgrt_vfprintf(_gpgrt__stream stream, String format, Object... ap);

    /**
     * Original signature : {@code int gpgrt_vfprintf_unlocked(gpgrt_stream_t, const char*, va_list)}
     */
    int gpgrt_vfprintf_unlocked(_gpgrt__stream stream, String format, Object... ap);

    /**
     * Original signature : {@code int gpgrt_asprintf(char**, const char*, null)}
     */
    int gpgrt_asprintf(PointerByReference r_buf, String format, Object... varArgs1);

    /**
     * Original signature : {@code int gpgrt_vasprintf(char**, const char*, va_list)}
     */
    int gpgrt_vasprintf(PointerByReference r_buf, String format, Object... ap);

    /**
     * Original signature : {@code char* gpgrt_bsprintf(const char*, null)}
     */
    Pointer gpgrt_bsprintf(String format, Object... varArgs1);

    /**
     * Original signature : {@code char* gpgrt_vbsprintf(const char*, va_list)}
     */
    Pointer gpgrt_vbsprintf(String format, Object... ap);

    /**
     * Original signature : {@code int gpgrt_snprintf(char*, size_t, const char*, null)}
     */
    int gpgrt_snprintf(ByteBuffer buf, NativeSize bufsize, String format, Object... varArgs1);

    /**
     * Original signature : {@code int gpgrt_vsnprintf(char*, size_t, const char*, va_list)}
     */
    int gpgrt_vsnprintf(ByteBuffer buf, NativeSize bufsize, String format, Object... arg_ptr);
}
