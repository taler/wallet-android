/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EccSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EddsaSignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Helper class for EdDSA signatures
 *
 * @author Oliver Broome
 */
public class EdDSASignature implements AutoCloseable {
    private GNUNET_CRYPTO_EccSignaturePurpose signaturePurposeStruct;
    private GNUNET_CRYPTO_EddsaSignature signatureStruct;

    /**
     * Creates a new EdDSA signature over the given message
     *
     * @param purpose    the purpose of the signature and the data to be signed.
     * @param privateKey the private key to sign the purpose with
     */
    public EdDSASignature(@NonNull ECCSignaturePurpose purpose, EdDSAPrivateKey privateKey) {
        this.signaturePurposeStruct = purpose.getStruct();
        this.signatureStruct = new GNUNET_CRYPTO_EddsaSignature();

        if (GNUNetLibrary.GNUNET_OK !=
                GNUNetLibrary.GNUNET_CRYPTO_eddsa_sign(
                        privateKey.getStruct(),
                        signaturePurposeStruct,
                        signatureStruct)) {
            throw new RuntimeException("Could not create signature, GNUNET_CRYPTO_eddsa_sign returned an error!");
        }
    }

    /**
     * Creates a new EdDSA signature from an encoded representation
     *
     * @param purpose       the purpose of the signature
     * @param gnunetEncoded the Base32-encoded signature structure
     */
    public EdDSASignature(@Nullable ECCSignaturePurpose purpose, @NonNull String gnunetEncoded) {
        if (purpose != null) {
            this.signaturePurposeStruct = purpose.getStruct();
        }

        this.signatureStruct = new GNUNET_CRYPTO_EddsaSignature(Codec.decode(gnunetEncoded));
        signatureStruct.read();
    }

    /**
     * Creates a new EdDSA signature from a blob, typically from a database
     *
     * @param purpose the signature Purpose, if available
     * @param blob    the blob
     */
    public EdDSASignature(@Nullable ECCSignaturePurpose purpose, byte[] blob) {
        if (blob.length != 64) {
            throw new IllegalArgumentException("The blob must by exactly 64 bytes long!");
        }

        if (purpose != null) {
            this.signaturePurposeStruct = purpose.getStruct();
        }

        byte[] r = new byte[32];
        byte[] s = new byte[32];

        System.arraycopy(blob, 0, r, 0, r.length);
        System.arraycopy(blob, 32, s, 0, s.length);

        this.signatureStruct = new GNUNET_CRYPTO_EddsaSignature(r, s);
    }

    /**
     * Wraps an existing GNUNET_CRYPTO_EddsaSignature.
     *
     * @param signatureStruct the signature struct to be wrapped
     */
    public EdDSASignature(GNUNET_CRYPTO_EddsaSignature signatureStruct) {
        this.signatureStruct = signatureStruct;
    }

    /**
     * Sets this signature's purpose
     *
     * @param signaturePurposeStruct the purpose to be set
     */
    public void setSignaturePurposeStruct(GNUNET_CRYPTO_EccSignaturePurpose signaturePurposeStruct) {
        this.signaturePurposeStruct = signaturePurposeStruct;
    }

    /**
     * Verifies that the signature signs the given ECCSignaturePurpose wrapper (the expected data and purpose)
     * and public key.
     *
     * @param signaturePurpose the ECCSignaturePurpose to verify against
     * @param publicKey        the public EdDSA to verify against
     * @return true, if the verification succeeded
     */
    public boolean verify(@NonNull ECCSignaturePurpose signaturePurpose, EdDSAPublicKey publicKey) {
        return GNUNetLibrary.GNUNET_CRYPTO_eddsa_verify(
                signaturePurpose.getPurposeCode(),
                signaturePurpose.getStruct(),
                signatureStruct,
                publicKey.getStruct()
        ) == GNUNetLibrary.GNUNET_OK;
    }

    /**
     * Gets this signature's underlying signature Structure
     *
     * @return the underlying GNUNET_CRYPTO_EddsaSignature Structure object
     */
    public GNUNET_CRYPTO_EddsaSignature getSignatureStruct() {
        return signatureStruct;
    }

    /**
     * Gets this signature's associated purpose
     *
     * @return the GNUNET_CRYPTO_EccSignaturePurpose Structure
     */
    public GNUNET_CRYPTO_EccSignaturePurpose getPurpose() {
        return signaturePurposeStruct;
    }

    /**
     * Encodes this signature to Base32
     *
     * @return the Base32-encoded signature as a String
     */
    public String encode() {
        return Codec.encode(signatureStruct);
    }

    @Override
    public void close() {
        if (signatureStruct != null) {
            MemoryUtils.freeStruct(signatureStruct);
            signatureStruct = null;
        }
    }

    /**
     * Generates a concatenated byte array of both structure fields (r, s)
     * suitable for storage in the database
     *
     * @return the signature Structure's r and s arrays, concatenated
     */
    public byte[] getBlob() {
        return signatureStruct.getPointer().getByteArray(0, 64);
    }
}
