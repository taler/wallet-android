/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.mint.wire.WireMethodEndpoint;

import java.io.IOException;
import java.net.URL;

/**
 * Represents the /wire/test REST API endpoint
 *
 * @author Oliver Broome
 */
public class TestWireEndpoint extends WireMethodEndpoint {

    /**
     * Creates a new TestWireEndpoint instance
     *
     * @param baseURL the base URL of the REST API
     * @throws IOException thrown if a connection could not be established
     */
    public TestWireEndpoint(@NonNull URL baseURL) throws IOException {
        super(baseURL, "/test");
    }

    @Override
    public void connect() throws IOException {
        //this endpoint redirects on success.
        sendRequest(302);
    }

    @Override
    protected void parseResults(@NonNull JsonReader reader) throws IOException {
        //not implemented because this feature does not do anything at the moment
    }

    @Override
    public boolean verify() {
        //there is no verification for this endpoint.
        return false;
    }
}
