/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.objects;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.gnunet.taler.wallet_android.database.model.Descriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TALER_CoinPublicInfo;

/**
 * Descriptor Object for coins
 *
 * @author Oliver Broome
 */
public class CoinDescriptor implements Descriptor, AutoCloseable {
    private long coinId;
    private long originId;
    private long reserveId;
    private DenominationDescriptor denomination;
    private EdDSAPublicKey publicKey;
    private EdDSAPrivateKey privateKey;
    private RSASignature signature;
    private AmountDescriptor currentValue;
    private boolean refreshed;

    public CoinDescriptor(@NonNull EdDSAPrivateKey privateKey,
                          @NonNull DenominationDescriptor denomination,
                          @Nullable AmountDescriptor currentValue,
                          @Nullable RSASignature signature,
                          boolean refreshed, long originId, long coinId, long reserveId) {
        this.coinId = coinId;
        this.originId = originId;
        this.denomination = denomination;
        this.privateKey = privateKey;
        this.signature = signature;
        this.currentValue = currentValue;
        this.refreshed = refreshed;
        this.reserveId = reserveId;
    }

    public long getCoinId() {
        return coinId;
    }

    public void setCoinId(long coinId) {
        this.coinId = coinId;
    }

    public DenominationDescriptor getDenomination() {
        return denomination;
    }

    public void setDenomination(DenominationDescriptor denomination) {
        this.denomination = denomination;
    }

    public EdDSAPublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(EdDSAPublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public EdDSAPrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(EdDSAPrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public AmountDescriptor getCurrentValue() {
        if (currentValue == null) {
            return denomination.getValue();
        } else {
            return currentValue;
        }
    }

    public void setCurrentValue(AmountDescriptor currentValue) {
        this.currentValue = currentValue;
    }

    @Override
    public void close() {
        if (denomination != null) {
            denomination.close();
            denomination = null;
        }

        if (publicKey != null) {
            publicKey.close();
            publicKey = null;
        }

        if (privateKey != null) {
            privateKey.close();
            privateKey = null;
        }
    }

    public long getOriginId() {
        return originId;
    }

    public boolean isRefreshed() {
        return refreshed;
    }

    public long getReserveId() {
        return reserveId;
    }

    public void setReserveId(long reserveId) {
        this.reserveId = reserveId;
    }

    public RSASignature getSignature() {
        return signature;
    }

    public void setSignature(RSASignature signature) {
        this.signature = signature;
    }

    public TALER_CoinPublicInfo getCoinPublicInfoStruct() {
        return new TALER_CoinPublicInfo(publicKey, denomination.getPublicKey(), signature);
    }
}
