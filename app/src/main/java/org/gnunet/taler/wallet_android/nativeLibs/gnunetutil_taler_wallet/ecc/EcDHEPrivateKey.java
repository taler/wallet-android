/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EcdhePrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Wrapper for ECDH private keys
 *
 * @author Oliver Broome
 */
public class EcDHEPrivateKey implements AutoCloseable {
    private GNUNET_CRYPTO_EcdhePrivateKey keyStruct;
    private EcDHEPublicKey publicKey;

    /**
     * Generates a new ECDH private key
     */
    public EcDHEPrivateKey() {
        keyStruct = GNUNetLibrary.GNUNET_CRYPTO_ecdhe_key_create();
    }

    /**
     * Creates a private EcDHE key from an encoded representation.
     *
     * @param gnunetEncoded the base32-encoded key data
     */
    public EcDHEPrivateKey(String gnunetEncoded) {
        this.keyStruct = new GNUNET_CRYPTO_EcdhePrivateKey(Codec.decode(gnunetEncoded));
        keyStruct.read();
    }

    /**
     * Generates the public key corresponding to this private key and returns it.
     *
     * @return the associated ECDHPublicKey
     */
    public EcDHEPublicKey getPublicKey() {
        if (publicKey == null && keyStruct != null) {
            publicKey = new EcDHEPublicKey(this);
        }
        if (publicKey != null) {
            return publicKey;
        }
        throw new RuntimeException("The memory associated with this key has been freed.");
    }

    /**
     * Gets the underlying Structure object of this private key
     *
     * @return the GNUNET_CRYPTO_EcdhePrivateKey Structure object
     */
    public GNUNET_CRYPTO_EcdhePrivateKey getStruct() {
        if (keyStruct != null) {
            return keyStruct;
        }

        throw new RuntimeException("The memory associated with this key has been freed.");
    }

    /**
     * Encodes the private key as a String, using Base32
     *
     * @return the encoded String
     */
    public String encode() {
        return Codec.encode(keyStruct);
    }

    /**
     * Frees the memory associated with this key (and it's public key, if available)
     */
    @Override
    public void close() {
        if (keyStruct != null) {
            GNUNetLibrary.GNUNET_CRYPTO_ecdhe_key_clear(keyStruct);
            MemoryUtils.freeStruct(keyStruct);
            keyStruct = null;
        }
        if (publicKey != null) {
            publicKey.close();
        }
    }
}
