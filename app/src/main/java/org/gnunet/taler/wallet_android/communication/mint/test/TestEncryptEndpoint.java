/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;

import org.gnunet.taler.wallet_android.communication.CommunicationConstants;
import org.gnunet.taler.wallet_android.communication.Endpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.CustomSaltBlock;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.InitialisationVector;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.SessionKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Represents the /test/encrypt endpoint of the mint API.
 *
 * @author Oliver Broome
 */
public class TestEncryptEndpoint extends Endpoint {
    private final String expectedResult;
    private String actualResult;

    public TestEncryptEndpoint(@NonNull URL baseURL, String secret, Hash keyMaterial) throws IOException {
        super(baseURL, MintEndpointConstants.TEST_PREFIX + "/encrypt", CommunicationConstants.METHOD_POST);

        CustomSaltBlock keySalt = new CustomSaltBlock("skey");
        CustomSaltBlock ivSalt = new CustomSaltBlock("iv");

        byte secretBytes[] = secret.getBytes(StandardCharsets.US_ASCII);

        Memory inputMemory = new Memory(secret.length());
        inputMemory.write(0, secretBytes, 0, secretBytes.length);

        try (SessionKey sessionKey = new SessionKey(
                keyMaterial.getHashStruct().getPointer(),
                keyMaterial.getNativeSize(),
                keySalt,
                new String[]{});

             InitialisationVector iv = new InitialisationVector(
                     keyMaterial.getHashStruct().getPointer(),
                     keyMaterial.getNativeSize(),
                     new NativeSize(32),
                     ivSalt,
                     new String[]{})) {

            Memory encryptedData = sessionKey.encrypt(
                    inputMemory,
                    new NativeSize(inputMemory.size()),
                    iv);

            expectedResult = Codec.encode(encryptedData);
        }

        PostJson postJson = new PostJson(Codec.encode(inputMemory), keyMaterial.getAsciiEncoded());

        Gson gson = new Gson();
        String postBody = gson.toJson(postJson);

        prepareRequestBody(postBody);
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        if ("output".equals(reader.nextName())) {
            actualResult = reader.nextString();
        } else {
            throw new UnknownJsonFieldException(reader);
        }

        reader.endObject();
    }

    public String getActualResult() {
        return actualResult;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    private class PostJson {
        private String input;
        private String key_hash;

        public PostJson(String input, String key_hash) {
            this.input = input;
            this.key_hash = key_hash;
        }
    }
}
