/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Wrapper class for messages blinded by GNUNET_CRYPTO_rsa_blind
 *
 * @author Oliver Broome
 */
public class RSABlindedMessage implements AutoCloseable {
    private NativeSize size;
    private Pointer data;

    /**
     * Blinds a hash's data
     *
     * @param blindingKey the key to use for blinding
     * @param publicKey   the public key of the signer
     */
    public RSABlindedMessage(Hash hashedMessage, RSABlindingKey blindingKey, RSAPublicKey publicKey) {
        if (hashedMessage != null && blindingKey != null && publicKey != null) {
            PointerByReference pointerByReference = new PointerByReference();

            this.size = GNUNetLibrary.GNUNET_CRYPTO_rsa_blind(
                    hashedMessage.getHashStruct(),
                    blindingKey.getStruct(),
                    publicKey.getStruct(),
                    pointerByReference);

            this.data = pointerByReference.getValue();
            MemoryUtils.freePointerRef(pointerByReference, false);
        } else {
            throw new RuntimeException("Arguments may not be null!");
        }
    }

    /**
     * Wrap a Pointer as a blinded message
     *
     * @param data the Pointer to the message's location in memory
     * @param size the length of the message
     */
    public RSABlindedMessage(Pointer data, NativeSize size) {
        if (data != null && size != null) {
            this.data = data;
            this.size = size;
        } else {
            throw new RuntimeException("Data and size cannot be null.");
        }
    }

    /**
     * Decodes a Base32-encoded blinded message
     *
     * @param gnunetEncoded the encoded data
     */
    public RSABlindedMessage(String gnunetEncoded) {
        Memory decoded = Codec.decode(gnunetEncoded);

        this.data = decoded;
        this.size = new NativeSize(decoded.size());
    }

    /**
     * Get the NativeSize of the blinded message
     *
     * @return the message's NativeSize
     */
    public NativeSize getNativeSize() {
        return size;
    }

    /**
     * Get the Pointer to the message
     *
     * @return pointer to the message's location in memory
     */
    public Pointer getPointer() {
        return data;
    }

    /**
     * Encodes the data present at the Pointer into a Base32 Crockford String
     *
     * @return the encoded String
     */
    public String encode() {
        if (data != null) {
            return Codec.encode(data, size);
        } else {
            throw new RuntimeException("no message in memory!");
        }
    }

    /**
     * Removes the message from memory.
     */
    @Override
    public void close() {
        if (data != null) {
            MemoryUtils.freePointer(data);
            data = null;
        }
    }
}
