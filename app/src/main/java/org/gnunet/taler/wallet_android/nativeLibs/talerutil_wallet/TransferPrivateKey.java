/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EcdhePrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;

/**
 * Wrapper class for private Taler Transfer Keys
 */
public class TransferPrivateKey {
    private GNUNET_CRYPTO_EcdhePrivateKey keyStruct;

    public TransferPrivateKey() {
        keyStruct = GNUNetLibrary.GNUNET_CRYPTO_ecdhe_key_create();
    }

    public TransferSecret getTransferSecret(TransferPublicKey publicKey) {
        return new TransferSecret(this, publicKey);
    }

    public GNUNET_CRYPTO_EcdhePrivateKey getStruct() {
        return keyStruct;
    }
}
