/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.tasks.mint;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.gnunet.taler.wallet_android.communication.mint.KeysEndpoint;
import org.gnunet.taler.wallet_android.database.DatabaseFactory;
import org.gnunet.taler.wallet_android.database.model.infrastructure.MintSigningKeyDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.DenominationDescriptor;
import org.gnunet.taler.wallet_android.database.storage.DenominationDatabase;
import org.gnunet.taler.wallet_android.database.storage.MintSigningKeysDatabase;
import org.gnunet.taler.wallet_android.database.storage.MintsDatabase;
import org.gnunet.taler.wallet_android.ui.MintListActivity;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Task for updating mint information
 *
 * @author Oliver Broome
 */
public class MintUpdateTask extends AsyncTask<Void, Integer, Boolean[]> {
    private WeakReference<MintListActivity> caller;

    public MintUpdateTask(WeakReference<MintListActivity> caller) {
        this.caller = caller;
    }

    protected static boolean[] saveSigningKeys(
            long mintId,
            MintSigningKeysDatabase database,
            List<MintSigningKeyDescriptor> signingKeys) {
        int signingKeyCount = signingKeys.size();
        boolean[] result = new boolean[signingKeyCount];

        for (int i = 0; i < signingKeyCount; i++) {
            MintSigningKeyDescriptor keyDescriptor = signingKeys.get(i);

            result[i] = database.addSigningKey(
                    mintId,
                    keyDescriptor.getSigningKey(),
                    keyDescriptor.getExpiry()) > -1;
        }

        return result;
    }

    protected static boolean[] saveDenominationKeys(
            long mintId,
            DenominationDatabase database,
            List<DenominationDescriptor> denominations) {
        int denominationCount = denominations.size();
        boolean[] result = new boolean[denominationCount];

        for (int i = 0; i < denominationCount; i++) {
            DenominationDescriptor denomination = denominations.get(i);

            result[i] = database.insertDenomination(mintId, denomination) > -1;
        }

        return result;
    }

    @Override
    protected Boolean[] doInBackground(Void... params) {
        Activity activity = caller.get();

        if (activity != null) {
            Context context = activity.getApplicationContext();

            MintsDatabase mintDatabase = DatabaseFactory.getMintsDatabase(context);
            MintSigningKeysDatabase signingKeysDatabase = DatabaseFactory.getMintSigningKeysDatabase(context);
            DenominationDatabase denominationDatabase = DatabaseFactory.getDenominationDatabase(context);

            try (MintsDatabase.Reader reader = mintDatabase.readerFor(mintDatabase.getAllMints(-1))) {
                Boolean[] result = new Boolean[reader.getCount()];
                int counter = 0;

                while (reader.getNext() != null) {
                    long currentMintId = reader.getCurrent().getMintID();
                    URL currentMintUrl = reader.getCurrent().getUrl();

                    try (KeysEndpoint endpoint = new KeysEndpoint(currentMintUrl)) {
                        endpoint.connect();

                        final List<MintSigningKeyDescriptor> signgningKeys = endpoint.getSigningKeys();
                        final List<DenominationDescriptor> denominationDescriptors = endpoint.getDenominations();

                        if (!signgningKeys.isEmpty()) {
                            saveSigningKeys(
                                    currentMintId,
                                    signingKeysDatabase,
                                    endpoint.getSigningKeys());
                        }

                        if (!denominationDescriptors.isEmpty()) {
                            saveDenominationKeys(
                                    currentMintId,
                                    denominationDatabase,
                                    denominationDescriptors);
                        }
                    } catch (MalformedURLException e) {
                        Log.e("taler_mint_update", "The the mint URL " + currentMintUrl + "was invalid!", e);
                        result[counter] = false;
                    } catch (IOException e) {
                        Log.e("taler_mint_update", "Received invalid data from the given URL!", e);
                    }

                    counter++;
                }

                return result;
            }
        } else {
            return new Boolean[0];
        }
    }
}
