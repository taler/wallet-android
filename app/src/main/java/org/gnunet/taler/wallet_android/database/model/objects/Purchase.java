/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.objects;

import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.database.model.Descriptor;
import org.gnunet.taler.wallet_android.database.model.state.ReserveHistoryDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;

import java.net.URL;
import java.util.List;

/**
 * Descriptor object for purchases
 *
 * @author Oliver Broome
 */
public class Purchase extends ReserveHistoryDescriptor implements Descriptor {
    private int purchaseID;
    private URL url;
    private int transactionID;
    private Hash wireHash;
    private String contract;
    private boolean checkedOut = false;
    private boolean purchaseConfirmed = false;
    private List<CoinDescriptor> coins;

    public Purchase(@NonNull AmountDescriptor amount) {
        super(amount);
    }

    public int getPurchaseID() {
        return purchaseID;
    }

    public void setPurchaseID(int purchaseID) {
        this.purchaseID = purchaseID;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public int getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(int transactionID) {
        this.transactionID = transactionID;
    }

    public Hash getWireHash() {
        return wireHash;
    }

    public void setWireHash(Hash wireHash) {
        this.wireHash = wireHash;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public boolean isCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }

    public boolean isPurchaseConfirmed() {
        return purchaseConfirmed;
    }

    public void setPurchaseConfirmed(boolean purchaseConfirmed) {
        this.purchaseConfirmed = purchaseConfirmed;
    }

    public List<CoinDescriptor> getCoins() {
        return coins;
    }

    public void setCoins(List<CoinDescriptor> coins) {
        this.coins = coins;
    }

    @Override
    public void close() {
        if (wireHash != null) {
            wireHash.close();
            wireHash = null;
        }
    }
}
