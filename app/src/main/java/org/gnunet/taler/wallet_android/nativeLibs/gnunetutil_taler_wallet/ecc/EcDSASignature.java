/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EccSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EcdsaSignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Helper class for EcDSA signatures
 *
 * @author Oliver Broome
 */
public class EcDSASignature implements AutoCloseable {
    private GNUNET_CRYPTO_EcdsaSignature signatureStruct;
    private GNUNET_CRYPTO_EccSignaturePurpose signaturePurposeStruct;

    /**
     * Creates a new EcDSA signature over the given message
     *
     * @param purpose    the purpose of the signature and the data to be signed.
     * @param privateKey the private key to sign the purpose with
     */
    public EcDSASignature(ECCSignaturePurpose purpose, EcDSAPrivateKey privateKey) {
        this.signaturePurposeStruct = purpose.getStruct();
        this.signatureStruct = new GNUNET_CRYPTO_EcdsaSignature();

        if (GNUNetLibrary.GNUNET_OK !=
                GNUNetLibrary.GNUNET_CRYPTO_ecdsa_sign(
                        privateKey.getStruct(),
                        signaturePurposeStruct,
                        signatureStruct)) {
            throw new RuntimeException("Could not create signature, GNUNET_CRYPTO_ecdsa_sign returned an error!");
        }
    }

    /**
     * Wraps an existing GNUNET_CRYPTO_EcdsaSignature.
     *
     * @param signatureStruct the signature struct to be wrapped
     */
    public EcDSASignature(GNUNET_CRYPTO_EcdsaSignature signatureStruct) {
        this.signatureStruct = signatureStruct;
    }

    /**
     * Verifies that the signature signs the given ECCSignaturePurpose wrapper (the expected data and purpose)
     * and public key
     *
     * @param signaturePurpose the ECCSignaturePurpose to verify against
     * @param publicKey        the public EcDSA to verify against
     * @return true, if the verification succeeded
     */
    public boolean verify(ECCSignaturePurpose signaturePurpose, EcDSAPublicKey publicKey) {
        return GNUNetLibrary.GNUNET_CRYPTO_ecdsa_verify(
                signaturePurpose.getPurposeCode(),
                signaturePurpose.getStruct(),
                signatureStruct,
                publicKey.getStruct()
        ) == GNUNetLibrary.GNUNET_OK;
    }

    /**
     * Gets the signature's purpose struct, if available
     *
     * @return a GNUNET_CRYPTO_EccSignaturePurpose
     */
    public GNUNET_CRYPTO_EccSignaturePurpose getPurpose() {
        return signaturePurposeStruct;
    }

    /**
     * Frees memory associated with this signature
     */
    @Override
    public void close() {
        if (signatureStruct != null) {
            MemoryUtils.freeStruct(signatureStruct);
            signatureStruct = null;
        }

        if (signaturePurposeStruct != null) {
            signaturePurposeStruct = null;
        }
    }

    public GNUNET_CRYPTO_EcdsaSignature getSignatureStruct() {
        return signatureStruct;
    }
}
