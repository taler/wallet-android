/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;

import org.gnunet.taler.wallet_android.communication.CommunicationConstants;
import org.gnunet.taler.wallet_android.communication.Endpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.CustomSaltBlock;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric.SessionKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Represents the /test/hkdf endpoint of the mint API.
 *
 * @author Oliver Broome
 */
public class TestHKDFEndpoint extends Endpoint {
    private final String expectedResult;
    private String actualResult;

    public TestHKDFEndpoint(@NonNull URL baseURL, String keyString) throws IOException {
        super(baseURL, MintEndpointConstants.TEST_PREFIX + "/hkdf", CommunicationConstants.METHOD_POST);

        byte[] keyBytes = keyString.getBytes(StandardCharsets.US_ASCII);

        Memory inputMemory = new Memory(keyString.length());
        inputMemory.write(0, keyBytes, 0, keyBytes.length);

        CustomSaltBlock salt = new CustomSaltBlock("salty");

        SessionKey sessionKey = new SessionKey(
                inputMemory,
                new NativeSize(inputMemory.size()),
                salt,
                new String[]{});

        expectedResult = sessionKey.encode();

        Gson gson = new Gson();

        PostJson postJson = new PostJson(Codec.encode(inputMemory));
        String postBody = gson.toJson(postJson);

        prepareRequestBody(postBody);
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        if ("output".equals(reader.nextName())) {
            actualResult = reader.nextString();
        } else {
            throw new UnknownJsonFieldException(reader);
        }

        reader.endObject();

        reader.close();
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public String getActualResult() {
        return actualResult;
    }

    private class PostJson {
        private String input;

        public PostJson(String input) {
            this.input = input;
        }
    }
}
