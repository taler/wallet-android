/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.objects;

import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;

/**
 * Descriptor class for SEPA wire transfer information
 *
 * @author Oliver Broome
 */
public class SepaDescriptor implements AutoCloseable {
    private String recipient;
    private String iban;
    private String bic;
    private EdDSASignature signature;

    public SepaDescriptor(@NonNull String recipient, @NonNull String iban, @NonNull String bic, EdDSASignature signature) {
        this.recipient = recipient;
        this.iban = iban;
        this.bic = bic;
        this.signature = signature;
    }

    public EdDSASignature getSignature() {
        return signature;
    }

    public void setSignature(EdDSASignature signature) {
        this.signature = signature;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    @Override
    public void close() {
        if (signature != null) {
            signature.close();
            signature = null;
        }
    }
}
