/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

import static org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary.GNUNET_CRYPTO_rsa_private_key_free;

/**
 * Helper class for private RSA keys
 *
 * @author Oliver Broome
 */
public class RSAPrivateKey implements AutoCloseable {
    /**
     * Refers to the GNUNET_CRYPTO_rsa_PrivateKey structure in memory
     */
    private GNUNetLibrary.GNUNET_CRYPTO_rsa_PrivateKey keyStruct;
    private RSAPublicKey publicKey;

    /**
     * Generates a new private RSA key with the desired key length (must be divisible by 8 and should be larger than or equal to 2048)
     *
     * @param keyLength The new private key's length in bits
     */
    public RSAPrivateKey(int keyLength) {
        if (keyLength % 8 != 0 || keyLength < 1024)
            throw new IllegalArgumentException("The RSA key length must be divisible by 8 and at least 1024!");

        keyStruct = GNUNetLibrary.GNUNET_CRYPTO_rsa_private_key_create(keyLength);
    }

    public RSAPrivateKey(GNUNetLibrary.GNUNET_CRYPTO_rsa_PrivateKey keyStruct) {
        this.keyStruct = keyStruct;
    }

    public RSAPrivateKey(String gnunetEncoded) {
        keyStruct = GNUNetLibrary.GNUNET_CRYPTO_rsa_private_key_decode(
                gnunetEncoded,
                new NativeSize(gnunetEncoded.getBytes().length));
        publicKey = new RSAPublicKey(gnunetEncoded, false);
    }

    /**
     * Encodes the key to a human-readable format
     *
     * @return the encoded public and private keys as a gcrypt S-expression
     * @throws RuntimeException if there is no reference to an in-memory key structure
     */
    public String encode() {
        if (keyStruct != null) {
            PointerByReference ptrRef = new PointerByReference();
            NativeSize encodedSize = GNUNetLibrary.GNUNET_CRYPTO_rsa_private_key_encode(keyStruct, ptrRef);

            Pointer encodedStringPointer = ptrRef.getValue();
            byte[] encodedStringBytes = encodedStringPointer.getByteArray(0, encodedSize.intValue() - 1);
            String result = Native.toString(encodedStringBytes);

            MemoryUtils.freePointerRef(ptrRef, true);

            return result;
        } else {
            throw new RuntimeException("No key in memory!");
        }
    }

    /**
     * Frees the resources associated with this private key
     */
    @Override
    public void close() {
        if (keyStruct != null || publicKey != null) {
            if (keyStruct != null) {
                GNUNET_CRYPTO_rsa_private_key_free(keyStruct);
                keyStruct = null;
            }

            if (publicKey != null) {
                publicKey.close();
                publicKey = null;
            }
        }
    }

    /**
     * Returns the wrapped Structure object of this private key
     *
     * @return the wrapped GNUNET_CRYPTO_rsa_PrivateKey Structure
     */
    public GNUNetLibrary.GNUNET_CRYPTO_rsa_PrivateKey getStruct() {
        return keyStruct;
    }

    /**
     * Returns the public key that corresponds to this private key. It is generated, if necessary.
     *
     * @return the corresponding RSAPublicKey
     */
    public RSAPublicKey getPublicKey() {
        if (publicKey == null && keyStruct != null) {
            this.publicKey = new RSAPublicKey(this);
        }
        return publicKey;
    }

    /**
     * Compares this private key with another
     *
     * @param otherPrivateKey the private key to compare this one with
     * @return true, if the two private keys are the same
     */
    public boolean compareTo(RSAPrivateKey otherPrivateKey) {
        return GNUNetLibrary.GNUNET_CRYPTO_rsa_private_key_cmp(keyStruct, otherPrivateKey.getStruct()) == 0;
    }
}
