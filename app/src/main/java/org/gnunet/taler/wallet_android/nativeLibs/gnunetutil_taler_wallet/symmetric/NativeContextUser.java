/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric;

import com.ochafik.lang.jnaerator.runtime.NativeSize;

import java.nio.charset.StandardCharsets;

/**
 * Convenience functionality for functions that use natively formatted contexts as arguments
 *
 * @author Oliver Broome
 */
public abstract class NativeContextUser {
    Object[] generateStringAndSizeVarargs(String[] contextArguments, boolean nullTerminated) {
        int contextLength = contextArguments.length;

        int resultLength = nullTerminated ? contextLength * 2 + 1 : contextLength * 2;

        Object[] result = new Object[resultLength];

        for (int i = 0; i < contextLength; i++) {
            result[i] = contextArguments[i];
            result[i + 1] = new NativeSize(contextArguments[i].getBytes(StandardCharsets.US_ASCII).length);
        }

        if (nullTerminated) {
            result[resultLength - 1] = null; //"null" terminator (as needed by GNUNET_CRYPTO_symmetric_derive_iv), hope this works as intended.
        }

        return result;
    }
}
