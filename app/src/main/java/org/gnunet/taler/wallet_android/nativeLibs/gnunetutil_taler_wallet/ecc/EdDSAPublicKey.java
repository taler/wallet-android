/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Pointer;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EddsaPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Helper class for EdDSA public keys
 *
 * @author Oliver Broome
 */
public class EdDSAPublicKey implements AutoCloseable {
    private GNUNET_CRYPTO_EddsaPublicKey keyStruct;

    /**
     * Generates the public EdDSA key for the given private key
     *
     * @param privateKey the private key the public key should be derived from
     */
    public EdDSAPublicKey(EdDSAPrivateKey privateKey) {
        keyStruct = new GNUNET_CRYPTO_EddsaPublicKey();
        GNUNetLibrary.GNUNET_CRYPTO_eddsa_key_get_public(
                privateKey.getStruct(),
                keyStruct);
    }

    /**
     * Creates a public EdDSA key from a String encoded with GNUNET_STRINGS_data_to_string
     *
     * @param gnunetEncoded the encoded String
     */
    public EdDSAPublicKey(String gnunetEncoded) {
        keyStruct = new GNUNET_CRYPTO_EddsaPublicKey();
        GNUNetLibrary.
                GNUNET_CRYPTO_eddsa_public_key_from_string(
                        gnunetEncoded,
                        new NativeSize(gnunetEncoded.getBytes().length),
                        keyStruct
                );
    }

    /**
     * Creates a public EdDSA key from an array of bytes, useful when reading from the database.
     *
     * @param blob the byte array containing the key data
     */
    public EdDSAPublicKey(byte[] blob) {
        keyStruct = new GNUNET_CRYPTO_EddsaPublicKey(blob);
    }

    /**
     * Removes the public key from memory.
     */
    @Override
    public void close() {
        if (keyStruct != null) {
            MemoryUtils.freeStruct(keyStruct);
            keyStruct = null;
        }
    }

    /**
     * Gets a String representation of the public key
     *
     * @return a String created with GNUNET_STRINGS_data_to_string
     */
    public String encode() {
        Pointer stringPointer = GNUNetLibrary.
                GNUNET_CRYPTO_eddsa_public_key_to_string(keyStruct);
        String result = stringPointer.getString(0);
        MemoryUtils.freePointer(stringPointer);
        return result;
    }

    public GNUNET_CRYPTO_EddsaPublicKey getStruct() {
        return keyStruct;
    }
}
