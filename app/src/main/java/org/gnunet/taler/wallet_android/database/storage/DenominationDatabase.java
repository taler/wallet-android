/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.DenominationDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPublicKey;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.ASC;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BLOB;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.CASCADE;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.CHECK;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.COMMA;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.IS_PARAMETER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.ON_DELETE;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.PRIMARY_KEY;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.REFERENCES;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.TEXT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.UNIQUE;

/**
 * Allows access to the denomination table
 *
 * @author Oliver Broome
 */
public class DenominationDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "denoms";

    public static final String COLUMN_MINT_ID = "mintid";
    public static final String COLUMN_DENOMINATION_PUBLIC_KEY = "denom_pub";

    public static final String COLUMN_MASTER_SIGNATURE = "master_sig";

    public static final String COLUMN_VAL_VALUE = "val_value";
    public static final String COLUMN_VAL_FRACTION = "val_fraction";
    public static final String COLUMN_VAL_CURRENCY = "val_currency";

    public static final String COLUMN_FEE_WITHDRAW_VALUE = "fee_withdraw_value";
    public static final String COLUMN_FEE_WITHDRAW_FRACTION = "fee_withdraw_fraction";
    public static final String COLUMN_FEE_WITHDRAW_CURRENCY = "fee_withdraw_currency";

    public static final String COLUMN_FEE_DEPOSIT_VALUE = "fee_deposit_value";
    public static final String COLUMN_FEE_DEPOSIT_FRACTION = "fee_deposit_fraction";
    public static final String COLUMN_FEE_DEPOSIT_CURRENCY = "fee_deposit_currency";

    public static final String COLUMN_FEE_REFRESH_VALUE = "fee_refresh_value";
    public static final String COLUMN_FEE_REFRESH_FRACTION = "fee_refresh_fraction";
    public static final String COLUMN_FEE_REFRESH_CURRENCY = "fee_refresh_currency";

    public static final String COLUMN_EXPIRE_WITHDRAW = "expire_withdraw";
    public static final String COLUMN_EXPIRE_DEPOSIT = "expire_deposit";
    public static final String COLUMN_EXPIRE_LEGAL = "expire_legal";

    public static final String COLUMN_START = "start";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + BEGIN_COLUMNS +
                    BaseColumns._ID + INTEGER + PRIMARY_KEY + NEXT_COLUMN +
                    COLUMN_MINT_ID + INTEGER + REFERENCES + MintsDatabase.TABLE_NAME + " (" + BaseColumns._ID + ")" + ON_DELETE + CASCADE + NEXT_COLUMN +
                    COLUMN_DENOMINATION_PUBLIC_KEY + TEXT + UNIQUE + NEXT_COLUMN +
                    COLUMN_MASTER_SIGNATURE + BLOB + CHECK + "(length(" + COLUMN_MASTER_SIGNATURE + ") = 64)" + NEXT_COLUMN +
                    COLUMN_VAL_VALUE + INTEGER + NEXT_COLUMN +
                    COLUMN_VAL_FRACTION + INTEGER + NEXT_COLUMN +
                    COLUMN_VAL_CURRENCY + TEXT + NEXT_COLUMN +
                    COLUMN_FEE_WITHDRAW_VALUE + INTEGER + NEXT_COLUMN +
                    COLUMN_FEE_WITHDRAW_FRACTION + INTEGER + NEXT_COLUMN +
                    COLUMN_FEE_WITHDRAW_CURRENCY + TEXT + NEXT_COLUMN +
                    COLUMN_FEE_DEPOSIT_VALUE + INTEGER + NEXT_COLUMN +
                    COLUMN_FEE_DEPOSIT_FRACTION + INTEGER + NEXT_COLUMN +
                    COLUMN_FEE_DEPOSIT_CURRENCY + TEXT + NEXT_COLUMN +
                    COLUMN_FEE_REFRESH_VALUE + INTEGER + NEXT_COLUMN +
                    COLUMN_FEE_REFRESH_FRACTION + INTEGER + NEXT_COLUMN +
                    COLUMN_FEE_REFRESH_CURRENCY + TEXT + NEXT_COLUMN +
                    COLUMN_EXPIRE_LEGAL + INTEGER + NEXT_COLUMN +
                    COLUMN_EXPIRE_WITHDRAW + INTEGER + NEXT_COLUMN +
                    COLUMN_EXPIRE_DEPOSIT + INTEGER + NEXT_COLUMN +
                    COLUMN_START + INTEGER +
                    END_COLUMNS;

    public static final String[] DENOMINATION_PROJECTION = new String[]{
            BaseColumns._ID,
            COLUMN_DENOMINATION_PUBLIC_KEY, COLUMN_MASTER_SIGNATURE,
            COLUMN_VAL_VALUE, COLUMN_VAL_FRACTION, COLUMN_VAL_CURRENCY,
            COLUMN_FEE_WITHDRAW_VALUE, COLUMN_FEE_WITHDRAW_FRACTION, COLUMN_FEE_WITHDRAW_CURRENCY,
            COLUMN_FEE_DEPOSIT_VALUE, COLUMN_FEE_DEPOSIT_FRACTION, COLUMN_FEE_DEPOSIT_CURRENCY,
            COLUMN_FEE_REFRESH_VALUE, COLUMN_FEE_REFRESH_FRACTION, COLUMN_FEE_REFRESH_CURRENCY,
            COLUMN_EXPIRE_LEGAL, COLUMN_EXPIRE_WITHDRAW, COLUMN_EXPIRE_DEPOSIT, COLUMN_START
    };

    public static final String[] ID_PROJECTION = new String[]{
            BaseColumns._ID
    };
    public static final String DENOMINATION_ID_WHERE = BaseColumns._ID + IS_PARAMETER;
    public static final String DENOMINATION_KEY_WHERE = COLUMN_DENOMINATION_PUBLIC_KEY + IS_PARAMETER;
    public static final String ORDER_BY_CUR_VAL_FRAC = COLUMN_VAL_CURRENCY + COMMA + COLUMN_VAL_VALUE + COMMA + COLUMN_VAL_FRACTION + ASC;
    private static final String MINT_ID_WHERE = COLUMN_MINT_ID + IS_PARAMETER;

    public DenominationDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }

    public long insertDenomination(long mintId, @NonNull DenominationDescriptor denominationDescriptor) {
        if (mintId == -1) {
            throw new IllegalArgumentException("No mint ID set!");
        }

        ContentValues content = new ContentValues(18);

        content.put(COLUMN_MINT_ID, mintId);

        content.put(COLUMN_DENOMINATION_PUBLIC_KEY, denominationDescriptor.getPublicKey().encode());
        content.put(COLUMN_MASTER_SIGNATURE, denominationDescriptor.getMasterSignature().getBlob());

        content.put(COLUMN_VAL_VALUE, denominationDescriptor.getValue().getValue());
        content.put(COLUMN_VAL_FRACTION, denominationDescriptor.getValue().getFraction());
        content.put(COLUMN_VAL_CURRENCY, denominationDescriptor.getValue().getCurrency());

        content.put(COLUMN_FEE_WITHDRAW_VALUE, denominationDescriptor.getWithdrawalFee().getValue());
        content.put(COLUMN_FEE_WITHDRAW_FRACTION, denominationDescriptor.getWithdrawalFee().getFraction());
        content.put(COLUMN_FEE_WITHDRAW_CURRENCY, denominationDescriptor.getWithdrawalFee().getCurrency());

        content.put(COLUMN_FEE_DEPOSIT_VALUE, denominationDescriptor.getDepositFee().getValue());
        content.put(COLUMN_FEE_DEPOSIT_FRACTION, denominationDescriptor.getDepositFee().getFraction());
        content.put(COLUMN_FEE_DEPOSIT_CURRENCY, denominationDescriptor.getDepositFee().getCurrency());

        content.put(COLUMN_FEE_REFRESH_VALUE, denominationDescriptor.getRefreshFee().getValue());
        content.put(COLUMN_FEE_REFRESH_FRACTION, denominationDescriptor.getRefreshFee().getFraction());
        content.put(COLUMN_FEE_REFRESH_CURRENCY, denominationDescriptor.getRefreshFee().getCurrency());

        content.put(COLUMN_EXPIRE_WITHDRAW, denominationDescriptor.getWithdrawalExpiry().getTimeInMillis());
        content.put(COLUMN_EXPIRE_DEPOSIT, denominationDescriptor.getDepositExpiry().getTimeInMillis());
        content.put(COLUMN_EXPIRE_LEGAL, denominationDescriptor.getLegalExpiry().getTimeInMillis());
        content.put(COLUMN_START, denominationDescriptor.getStart().getTimeInMillis());

        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            return database.replace(TABLE_NAME, null, content);
        }
    }

    public DenominationDescriptor getDenominationForId(long denominationId) {
        try (SQLiteDatabase database = databaseHelper.getReadableDatabase();
             Reader reader = readerFor(database.query(TABLE_NAME,
                     DENOMINATION_PROJECTION,
                     DENOMINATION_ID_WHERE,
                     new String[]{Long.toString(denominationId)},
                     null, null, null))) {
            return reader.getNext();
        }
    }

    public Cursor getDenominationsForMint(long mintId) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        return database.query(TABLE_NAME,
                DENOMINATION_PROJECTION,
                MINT_ID_WHERE,
                new String[]{Long.toString(mintId)},
                null, null,
                ORDER_BY_CUR_VAL_FRAC);
    }

    public List<String> getCurrenciesForMint(long mintId) {
        try (Reader reader = readerFor(getDenominationsForMint(mintId))) {
            List<String> result = new ArrayList<>(reader.getCount());
            while (reader.getNext() != null) {
                result.add(reader.getCurrent().getValue().getCurrency());
            }
            return Collections.unmodifiableList(result);
        }
    }

    public long getIdForKey(@NonNull RSAPublicKey key) {
        return getIdForKey(key.encode());
    }

    public long getIdForKey(@NonNull String encodedKey) {
        try (DenominationDescriptor denomination = getDenominationForKey(encodedKey)) {
            if (denomination != null) {
                return denomination.getId();
            } else {
                return -1;
            }
        }
    }

    public DenominationDescriptor getDenominationForKey(@NonNull RSAPrivateKey key) {
        return getDenominationForKey(key.encode());
    }

    public DenominationDescriptor getDenominationForKey(@NonNull String encodedRSAKey) {
        try (SQLiteDatabase database = databaseHelper.getReadableDatabase();
             Reader resultReader = readerFor(database.query(TABLE_NAME,
                     ID_PROJECTION,
                     DENOMINATION_KEY_WHERE,
                     new String[]{encodedRSAKey},
                     null, null, null, null))) {

            if (resultReader.getNext() != null) {
                return resultReader.getCurrent();
            }

            return null;
        }
    }

    public Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }


    public class Reader implements AutoCloseable {
        private final Cursor cursor;

        public Reader(Cursor cursor) {
            this.cursor = cursor;
        }

        public DenominationDescriptor getNext() {
            if (cursor == null || !cursor.moveToNext()) {
                return null;
            } else {
                return getCurrent();
            }
        }

        private DenominationDescriptor getCurrent() {
            RSAPublicKey key = new RSAPublicKey(cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_DENOMINATION_PUBLIC_KEY)), false);
            EdDSASignature signature = new EdDSASignature(null, cursor.getBlob(cursor.getColumnIndexOrThrow(COLUMN_MASTER_SIGNATURE)));

            AmountDescriptor value = new AmountDescriptor(
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_VAL_CURRENCY)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_VAL_VALUE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_VAL_FRACTION))
            );
            AmountDescriptor depositFee = new AmountDescriptor(
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FEE_DEPOSIT_CURRENCY)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_FEE_DEPOSIT_VALUE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_FEE_DEPOSIT_FRACTION))
            );
            AmountDescriptor refreshFee = new AmountDescriptor(
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FEE_REFRESH_CURRENCY)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_FEE_REFRESH_VALUE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_FEE_REFRESH_FRACTION))
            );
            AmountDescriptor withdrawFee = new AmountDescriptor(
                    cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_FEE_WITHDRAW_CURRENCY)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_FEE_WITHDRAW_VALUE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_FEE_WITHDRAW_FRACTION))
            );

            long start = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_START));
            long depositExpiry = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_EXPIRE_DEPOSIT));
            long legalExpiry = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_EXPIRE_LEGAL));
            long withdrawExpiry = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_EXPIRE_WITHDRAW));

            return new DenominationDescriptor(key, signature,
                    value, depositFee, refreshFee, withdrawFee,
                    start, depositExpiry, legalExpiry, withdrawExpiry);
        }

        public int getCount() {
            if (cursor == null) {
                return 0;
            } else {
                return cursor.getCount();
            }
        }

        @Override
        public void close() {
            cursor.close();
        }
    }
}
