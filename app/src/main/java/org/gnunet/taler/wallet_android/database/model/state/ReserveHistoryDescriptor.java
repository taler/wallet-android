/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.state;

import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.database.model.Descriptor;
import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;

/**
 * Abstract Descriptor object for withdrawal history items
 *
 * @author Oliver Broome
 */
public abstract class ReserveHistoryDescriptor implements Descriptor, AutoCloseable {
    private AmountDescriptor amount;

    public ReserveHistoryDescriptor(@NonNull AmountDescriptor amount) {
        this.amount = amount;
    }

    public AmountDescriptor getAmount() {
        return amount;
    }

    public void setAmount(AmountDescriptor amount) {
        this.amount = amount;
    }
}
