/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.tasks.reserve;

import android.os.AsyncTask;

import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.ReserveDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;
import org.gnunet.taler.wallet_android.ui.fragments.ReserveGeneratorWorkerFragment;

import java.lang.ref.WeakReference;

/**
 * Task for generating a ReserveDescriptor with a keypair
 *
 * @author Oliver Broome
 */
public class ReservePrepareTask extends AsyncTask<Void, Integer, ReserveDescriptor> {
    private EdDSAPrivateKey privateKey;
    private long mintId;
    private AmountDescriptor amount;
    private RSABlindingKey blindingKey;

    private WeakReference<ReserveGeneratorWorkerFragment> caller;

    public ReservePrepareTask(long mintId, AmountDescriptor amount, int blindingKeyLength,
                              WeakReference<ReserveGeneratorWorkerFragment> caller) {
        this.caller = caller;
        this.privateKey = new EdDSAPrivateKey();
        this.mintId = mintId;
        this.amount = amount;
        this.blindingKey = new RSABlindingKey(blindingKeyLength);
    }

    @Override
    protected ReserveDescriptor doInBackground(Void... params) {
        ReserveDescriptor result;

        privateKey = new EdDSAPrivateKey();

        result = new ReserveDescriptor(-1,
                mintId,
                privateKey,
                privateKey.getPublicKey(),
                amount,
                amount,
                blindingKey,
                null);

        return result;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(ReserveDescriptor reserve) {
        super.onPostExecute(reserve);

        ReserveGeneratorWorkerFragment parent = caller.get();

        if (parent != null) {
            parent.persistReserve(reserve);
        }
    }
}
