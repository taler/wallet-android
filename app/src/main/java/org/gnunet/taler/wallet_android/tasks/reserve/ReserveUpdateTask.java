/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.tasks.reserve;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import org.gnunet.taler.wallet_android.communication.mint.reserve.ReserveStatusEndpoint;
import org.gnunet.taler.wallet_android.database.DatabaseFactory;
import org.gnunet.taler.wallet_android.database.model.infrastructure.MintDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.Purchase;
import org.gnunet.taler.wallet_android.database.model.objects.ReserveDescriptor;
import org.gnunet.taler.wallet_android.database.model.state.ReserveHistoryDescriptor;
import org.gnunet.taler.wallet_android.database.model.state.WithdrawalHistoryDescriptor;
import org.gnunet.taler.wallet_android.database.storage.MintsDatabase;
import org.gnunet.taler.wallet_android.database.storage.ReserveDatabase;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

/**
 * Service that updates information on existing reserves
 *
 * @author Oliver Broome
 */
public class ReserveUpdateTask extends AsyncTask<Void, Integer, Boolean> {
    public void updateReserves(@NonNull Context context) {
        MintsDatabase mintsDB = DatabaseFactory.getMintsDatabase(context);
        ReserveDatabase reserveDB = DatabaseFactory.getReserveDatabase(context);

        try (MintsDatabase.Reader mintReader = mintsDB.readerFor(mintsDB.getAllMints(-1))) {
            while (mintReader.getNext() != null) {
                MintDescriptor currentMint = mintReader.getCurrent();

                try (ReserveDatabase.Reader reserveReader =
                             reserveDB.readerFor(
                                     reserveDB.getReservesForMint(currentMint.getMintID()))) {

                    while (reserveReader.getNext() != null) {
                        ReserveDescriptor currentReserve = reserveReader.getCurrent();
                        ReserveStatusEndpoint reserveEndpoint =
                                new ReserveStatusEndpoint(
                                        currentMint.getUrl(),
                                        currentReserve.getPublicKey());
                        reserveEndpoint.connect();

                        List<ReserveHistoryDescriptor> reserveHistory = reserveEndpoint.getReserveHistory();

                        for (ReserveHistoryDescriptor historyItem : reserveHistory) {
                            if (historyItem instanceof WithdrawalHistoryDescriptor) {
                                saveWithdrawalDescriptor((WithdrawalHistoryDescriptor) historyItem, context);
                            } else {
                                savePurchaseDescriptor((Purchase) historyItem, context);
                            }
                        }

                        reserveDB.updateBalance(
                                currentReserve.getReserveID(),
                                reserveEndpoint.getBalance());
                    }
                } catch (MalformedURLException e) {
                    Log.e("taler_reserve_update", "The the mint URL " + currentMint.getUrl() + "was invalid!", e);
                } catch (IOException e) {
                    Log.e("taler_reserve_update", "Received invalid data from the given URL!", e);
                }
            }
        }
    }

    private void saveWithdrawalDescriptor(WithdrawalHistoryDescriptor descriptor, Context context) {

    }

    private void savePurchaseDescriptor(Purchase descriptor, Context context) {

    }

    @Override
    protected Boolean doInBackground(Void... params) {
        return null;
    }
}
