/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EcdsaPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Helper class for EcDSA private keys
 *
 * @author Oliver Broome
 */
public class EcDSAPrivateKey implements AutoCloseable {
    private GNUNET_CRYPTO_EcdsaPrivateKey keyStruct;
    private EcDSAPublicKey publicKey;

    /**
     * Creates a new EcDSA private key
     */
    public EcDSAPrivateKey() {
        keyStruct = GNUNetLibrary.GNUNET_CRYPTO_ecdsa_key_create();
    }

    /**
     * Derives a new private key from an existing private key
     *
     * @param oldPrivateKey the existing key you want to derive the new one from
     * @param label         a label for the new key
     * @param context       context in which the key should be used
     */
    public EcDSAPrivateKey(EcDSAPrivateKey oldPrivateKey, String label, String context) {
        keyStruct = GNUNetLibrary.GNUNET_CRYPTO_ecdsa_private_key_derive(
                oldPrivateKey.getStruct(),
                label,
                context);
    }

    /**
     * Wraps an existing EcDSA private key in memory
     *
     * @param keyStruct the Pointer to the private key's location
     */
    public EcDSAPrivateKey(GNUNET_CRYPTO_EcdsaPrivateKey keyStruct) {
        this.keyStruct = keyStruct;
    }

    /**
     * Creates the public key corresponding to this key (if necessary) and returns it
     *
     * @return the corresponding EcDSA public key
     */
    public EcDSAPublicKey getPublicKey() {
        if (publicKey == null && keyStruct != null) {
            publicKey = new EcDSAPublicKey(this);
        }
        return publicKey;
    }

    /**
     * Returns the private key's Structure object
     *
     * @return the GNUNET_CRYPTO_EcdsaPrivateKey Structure
     */
    public GNUNET_CRYPTO_EcdsaPrivateKey getStruct() {
        return keyStruct;
    }

    /**
     * Removes the private key from memory if it is present.
     * Will also close() the associated public key, if available.
     */
    @Override
    public void close() {
        if (keyStruct != null) {
            GNUNetLibrary.GNUNET_CRYPTO_ecdsa_key_clear(keyStruct);
            MemoryUtils.freeStruct(keyStruct);
            keyStruct = null;
        }

        if (publicKey != null) {
            publicKey.close();
            publicKey = null;
        }
    }


}
