/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.state;

import org.gnunet.taler.wallet_android.database.model.objects.CoinDescriptor;

import java.util.List;

/**
 * Descriptor for the RefreshMelt table
 *
 * @author Oliver Broome
 */
public class RefreshMelt {
    /**
     * The Refresh this RefreshMelt belongs to
     */
    private Refresh refresh;

    /**
     * The coins belonging to this RefreshMelt
     */
    private List<CoinDescriptor> coins;

    /**
     * The coin that the new coins originate from
     */
    private CoinDescriptor oldCoin;

    public Refresh getRefresh() {
        return refresh;
    }

    public void setRefresh(Refresh refresh) {
        this.refresh = refresh;
    }

    public List<CoinDescriptor> getCoins() {
        return coins;
    }

    public void setCoins(List<CoinDescriptor> coins) {
        this.coins = coins;
    }

    public CoinDescriptor getOldCoin() {
        return oldCoin;
    }

    public void setOldCoin(CoinDescriptor oldCoin) {
        this.oldCoin = oldCoin;
    }
}
