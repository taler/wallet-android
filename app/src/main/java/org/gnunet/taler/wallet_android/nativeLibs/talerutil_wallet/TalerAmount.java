/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet;

import android.support.annotation.NonNull;

import com.sun.jna.Native;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TALER_Amount;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Wrapper class for Taler Amount structs
 *
 * @author Oliver Broome
 */
public class TalerAmount implements AutoCloseable, Comparable<TalerAmount> {
    private TALER_Amount amountStruct;

    public TalerAmount(long value, int fraction, String currency) {
        amountStruct = new TALER_Amount(value, fraction, Native.toByteArray(currency, "ASCII"));
    }

    /**
     * Wraps an existing Taler Amount Structure object
     *
     * @param amountStruct the TALER_Amount Structure to be wrapped
     */
    public TalerAmount(TALER_Amount amountStruct) {
        this.amountStruct = amountStruct;
    }

    public void add(TalerAmount otherAmount, boolean merge) throws IncompatibleCurrencyException {
        TALER_Amount result = new TALER_Amount.ByReference();
        if (TalerLibrary.TALER_amount_cmp_currency(amountStruct, otherAmount.getStruct()) == GNUNetLibrary.GNUNET_YES) {
            if (TalerLibrary.TALER_amount_add(result, amountStruct, otherAmount.getStruct()) == GNUNetLibrary.GNUNET_OK) {
                MemoryUtils.freeStruct(amountStruct);
                amountStruct = result;

                if (merge) {
                    otherAmount.close();
                }
            } else {
                MemoryUtils.freeStruct(result);
                throw new RuntimeException("The addition caused an overflow!");
            }
        } else {
            MemoryUtils.freeStruct(result);
            throw new IncompatibleCurrencyException("Incompatible currencies!");
        }
    }

    public void subtract(TalerAmount otherAmount, boolean merge) throws IncompatibleCurrencyException {
        TALER_Amount result = new TALER_Amount.ByReference();
        if (TalerLibrary.TALER_amount_cmp_currency(amountStruct, otherAmount.getStruct()) == GNUNetLibrary.GNUNET_YES) {
            if (TalerLibrary.TALER_amount_subtract(result, amountStruct, otherAmount.getStruct()) == GNUNetLibrary.GNUNET_OK) {
                MemoryUtils.freeStruct(amountStruct);
                amountStruct = result;

                if (merge) {
                    otherAmount.close();
                }
            } else {
                MemoryUtils.freeStruct(result);
                throw new RuntimeException("The subtraction failed!");
            }
        } else {
            MemoryUtils.freeStruct(result);
            throw new IncompatibleCurrencyException("Incompatible currencies!");
        }
    }

    public TALER_Amount getStruct() {
        return amountStruct;
    }

    @Override
    public void close() {
        if (amountStruct != null) {
            MemoryUtils.freeStruct(amountStruct);
            amountStruct = null;
        }
    }

    @Override
    public int compareTo(@NonNull TalerAmount otherAmount) {
        final TALER_Amount otherAmountStruct = otherAmount.getStruct();
        if (TalerLibrary.TALER_amount_cmp_currency(amountStruct, otherAmountStruct) == GNUNetLibrary.GNUNET_YES) {
            return TalerLibrary.TALER_amount_cmp(amountStruct, otherAmountStruct);
        } else {
            throw new RuntimeException("Could not compare these two Amounts - they have different currencies!");
        }
    }

    private class IncompatibleCurrencyException extends Throwable {
        public IncompatibleCurrencyException(String message) {
            super(message);
        }
    }
}
