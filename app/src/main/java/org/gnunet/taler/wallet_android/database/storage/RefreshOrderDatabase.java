/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NOT_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.REFERENCES;

/**
 * Allows access to the refresh_order table
 *
 * @author Oliver Broome
 */
public class RefreshOrderDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "refresh_order";

    public static final String COLUMN_REFRESH_ID = "refreshid";
    public static final String COLUMN_DENOMINATION_ID = "denomid";
    public static final String COLUMN_NEW_COIN_INDEX = "newcoin_index";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + BEGIN_COLUMNS +
                    COLUMN_REFRESH_ID + INTEGER + REFERENCES + RefreshDatabase.TABLE_NAME + " (" + BaseColumns._ID + ")" + NEXT_COLUMN +
                    COLUMN_DENOMINATION_ID + INTEGER + REFERENCES + DenominationDatabase.TABLE_NAME + " (" + BaseColumns._ID + ")" + NEXT_COLUMN +
                    COLUMN_NEW_COIN_INDEX + INTEGER + NOT_NULL +
                    END_COLUMNS;

    public RefreshOrderDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }
}
