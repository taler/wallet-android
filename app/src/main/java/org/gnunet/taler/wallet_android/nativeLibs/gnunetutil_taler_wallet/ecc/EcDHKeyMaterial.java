/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_HashCode;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Wrapper for the GNUnet EcDH key material generation function
 *
 * @author Oliver Broome
 */
public class EcDHKeyMaterial implements AutoCloseable {
    private GNUNET_HashCode keyMaterial;

    public EcDHKeyMaterial(EcDHEPrivateKey privateKey, EcDHEPublicKey publicKey) {
        GNUNET_HashCode result = new GNUNET_HashCode.ByReference();
        GNUNetLibrary.GNUNET_CRYPTO_ecc_ecdh(privateKey.getStruct(), publicKey.getStruct(), result);

        keyMaterial = result;
    }

    public EcDHKeyMaterial(String gnunetEncoded) {
        keyMaterial = new GNUNET_HashCode(Codec.decode(gnunetEncoded));
    }

    public String encode() {
        return Codec.encode(keyMaterial);
    }

    public GNUNET_HashCode getKeyMaterial() {
        return keyMaterial;
    }

    @Override
    public void close() {
        if (keyMaterial != null) {
            keyMaterial.clear();
            MemoryUtils.freeStruct(keyMaterial);
            keyMaterial = null;
        }
    }
}
