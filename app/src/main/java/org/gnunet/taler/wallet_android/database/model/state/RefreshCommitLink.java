/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.state;

import org.gnunet.taler.wallet_android.database.model.objects.CoinDescriptor;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Descriptor for the RefreshCommitLink table
 */
public class RefreshCommitLink {
    /**
     * The Refresh this RefreshCommitLink belongs to
     */
    private Refresh refresh;

    /**
     * //TODO: Find out what this is
     */
    private int cncIndex;

    /**
     * The original Coin for this RefreshCommitLink
     */
    private CoinDescriptor oldCoin;

    /**
     * The private key for this transfer
     */
    private PrivateKey privateKey;

    /**
     * The public key for this transfer
     */
    private PublicKey publicKey;

    /**
     * The secret for this RefreshCommitLink
     */
    private byte[] secret;

    public Refresh getRefresh() {
        return refresh;
    }

    public void setRefresh(Refresh refresh) {
        this.refresh = refresh;
    }

    public int getCncIndex() {
        return cncIndex;
    }

    public void setCncIndex(int cncIndex) {
        this.cncIndex = cncIndex;
    }

    public CoinDescriptor getOldCoin() {
        return oldCoin;
    }

    public void setOldCoin(CoinDescriptor oldCoin) {
        this.oldCoin = oldCoin;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public byte[] getSecret() {
        return secret;
    }

    public void setSecret(byte[] secret) {
        this.secret = secret;
    }
}
