/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.ui.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.ReserveDescriptor;
import org.gnunet.taler.wallet_android.tasks.reserve.ReserveAddTask;
import org.gnunet.taler.wallet_android.tasks.reserve.ReservePrepareTask;

import java.lang.ref.WeakReference;

/**
 * A helper fragment for generating reserves
 *
 * @author Oliver Broome
 */
public class ReserveGeneratorWorkerFragment extends Fragment {
    private Callbacks listener;

    public ReserveGeneratorWorkerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }

    public void generateKey(long mintId, AmountDescriptor amount) {
        AsyncTask generateKeysTask =
                new ReservePrepareTask(mintId, amount, 2048, new WeakReference<>(this)).execute();
    }

    public void persistReserve(ReserveDescriptor reserve) {
        AsyncTask persistKeysTask =
                new ReserveAddTask(new WeakReference<>(this))
                        .execute(reserve);
    }

    public void communicateResult(ReserveDescriptor result) {
        listener.onReservePersisted(result);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (Callbacks) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement Callbacks!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface Callbacks {
        void onReservePersisted(ReserveDescriptor result);
    }

}
