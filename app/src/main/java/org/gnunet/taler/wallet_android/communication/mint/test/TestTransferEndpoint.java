/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.CommunicationConstants;
import org.gnunet.taler.wallet_android.communication.Endpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants;

import java.io.IOException;
import java.net.URL;

/**
 * Represents the /test/transfer endpoint of the REST API
 *
 * @author Oliver Broome
 */
public class TestTransferEndpoint extends Endpoint {
    public TestTransferEndpoint(@NonNull URL baseURL) throws IOException {
        super(baseURL, MintEndpointConstants.TEST_PREFIX + "/transfer", CommunicationConstants.METHOD_POST);
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        switch (reader.nextString()) {
            case "secret":
                break;
            default:
                throw new UnknownJsonFieldException(reader);
        }

        reader.endObject();

        reader.close();
    }

    private class JsonData {
        private String secret_enc;
        private String trans_priv;
        private String coin_pub;

        public JsonData(String secret_enc, String trans_priv, String coin_pub) {
            this.secret_enc = secret_enc;
            this.trans_priv = trans_priv;
            this.coin_pub = coin_pub;
        }
    }
}
