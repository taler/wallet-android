/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.test;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.CommunicationConstants;
import org.gnunet.taler.wallet_android.communication.Endpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindedMessage;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSASignature;

import java.io.IOException;
import java.net.URL;

/**
 * Represents the /test/rsa/sign endpoint of the mint API.
 * Consumes a blinded value and returns a signature over that value.
 *
 * @author Oliver Broome
 */
public class TestRSASignEndpoint extends Endpoint {
    private RSASignature receivedSignature;

    public TestRSASignEndpoint(@NonNull URL baseURL, RSABlindedMessage blindedMessage) throws IOException {
        super(baseURL, MintEndpointConstants.TEST_PREFIX + "/rsa/sign", CommunicationConstants.METHOD_POST);

        PostData postData = new PostData(blindedMessage.encode());

        Gson gson = new Gson();
        String jsonData = gson.toJson(postData);

        prepareRequestBody(jsonData);
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        if ("rsa_blind_sig".equals(reader.nextName())) {
            receivedSignature = new RSASignature(reader.nextString(), true);
        } else {
            throw new UnknownJsonFieldException(reader);
        }

        reader.endObject();

        reader.close();
    }

    public RSASignature getReceivedSignature() {
        return receivedSignature;
    }

    private class PostData {
        private String blind_ev;

        private PostData(String blind_ev) {
            this.blind_ev = blind_ev;
        }
    }
}
