/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.FunctionalEndpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.database.model.infrastructure.MintSigningKeyDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.AuditedDenomination;
import org.gnunet.taler.wallet_android.database.model.objects.AuditorDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.DenominationDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.HashContext;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Represents the /keys endpoint of the mint API.
 * <p/>
 * Allows the wallet to retrieve the mints public master key, a list of all denominations offered,
 * and a list of the mint's signing keys.
 *
 * @author Oliver Broome
 */
public class KeysEndpoint extends FunctionalEndpoint {
    private EdDSASignature responseSignature;
    private EdDSAPublicKey publicMasterKey;
    private EdDSAPublicKey responseSigningKey;

    private Calendar issueDate;

    private List<DenominationDescriptor> denominations;
    private List<MintSigningKeyDescriptor> signingKeys;
    private List<AuditorDescriptor> auditors;

    /**
     * initialises the signing key endpoint for the given mint
     *
     * @param baseURL the mint's base URL
     * @throws IOException
     */
    public KeysEndpoint(@NonNull URL baseURL) throws IOException {
        super(baseURL, "/keys", "GET");
    }

    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();   //root
        while (reader.hasNext()) {  //iterate through the root items
            switch (reader.nextName()) {  //step to the next property name
                case "master_public_key":
                    publicMasterKey = new EdDSAPublicKey(reader.nextString());
                    break;
                case "eddsa_sig":
                    responseSignature = new EdDSASignature(null, reader.nextString());
                    break;
                case "eddsa_pub":
                    responseSigningKey = new EdDSAPublicKey(reader.nextString());
                    break;
                case "denoms":
                    denominations = parseDenominations(reader);
                    break;
                case "signkeys":
                    signingKeys = parseSigningKeys(reader);
                    break;
                case "list_issue_date":
                    issueDate = parseDateToCalendar(reader);
                    break;
                case "auditors":
                    auditors = parseAuditors(reader);
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        reader.close();
    }

    /**
     * parses the auditors of the mint and their audited denominations
     *
     * @param reader the reader pointing to the aditors array
     * @return a list of AuditorDescriptors
     * @throws IOException
     */
    private List<AuditorDescriptor> parseAuditors(@NonNull JsonReader reader) throws IOException {
        reader.beginArray();

        List<AuditorDescriptor> result = new ArrayList<>();

        while (reader.hasNext()) {
            result.add(parseAuditor(reader));
        }

        reader.endArray();

        return result;
    }

    /**
     * parses an auditor's audited denominations
     *
     * @param reader the reader pointing to the auditor object
     * @return the auditor descriptor
     * @throws IOException
     */
    private AuditorDescriptor parseAuditor(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        EdDSAPublicKey publicKey = null;
        List<AuditedDenomination> auditedDenominations = null;

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "auditor_pub":
                    publicKey = new EdDSAPublicKey(reader.nextString());
                    break;
                case "denomination_keys":
                    auditedDenominations = parseAuditedDenominations(reader);
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        return new AuditorDescriptor(publicKey, auditedDenominations);
    }

    /**
     * parses an array of audited denominations
     *
     * @param reader the reader pointing to the array of audited denominations
     * @return a list of audited denominations
     * @throws IOException
     */
    private List<AuditedDenomination> parseAuditedDenominations(@NonNull JsonReader reader) throws IOException {
        reader.beginArray();

        List<AuditedDenomination> result = new ArrayList<>();

        while (reader.hasNext()) {
            result.add(parseAuditedDenomination(reader));
        }

        reader.endArray();

        return result;
    }

    /**
     * parses an audited denomination
     *
     * @param reader the reader pointing to the audited denomination
     * @return the audited denomination
     * @throws IOException
     */
    private AuditedDenomination parseAuditedDenomination(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        Hash denominationKeyHash = null;
        EdDSASignature signature = null;

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "denom_pub_h":
                    denominationKeyHash = new Hash(reader.nextString());
                    break;
                case "auditor_sig":
                    signature = new EdDSASignature(null, reader.nextString());
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        return new AuditedDenomination(denominationKeyHash, signature);
    }

    /**
     * parses an array of signing key descriptors
     *
     * @param reader the reader pointing to the signing key array
     * @return a list of signing key descriptors
     * @throws IOException
     */
    private List<MintSigningKeyDescriptor> parseSigningKeys(@NonNull JsonReader reader) throws IOException {
        reader.beginArray();    //enter the key array

        List<MintSigningKeyDescriptor> result = new ArrayList<>();

        while (reader.hasNext()) {  //check to see if there are any more items
            result.add(parseSigningKey(reader));
        }

        reader.endArray();      //leave the key array

        return result;
    }

    /**
     * parses a signing key object
     *
     * @param reader the reader the reader pointing to the signing key object
     * @return the signing key descriptor
     * @throws IOException
     */
    private MintSigningKeyDescriptor parseSigningKey(@NonNull JsonReader reader) throws IOException {
        EdDSAPublicKey publicSigningKey = null, mintMasterPublicKey = null;
        EdDSASignature signature = null;
        long start = 0L, expiry = 0L, end = 0L;

        reader.beginObject();

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "key":
                    publicSigningKey = new EdDSAPublicKey(reader.nextString());
                    break;
                case "master_sig":
                    signature = new EdDSASignature(null, reader.nextString());
                    break;
                case "master_pub":
                    mintMasterPublicKey = new EdDSAPublicKey(reader.nextString());
                    break;
                case "stamp_start":
                    start = parseDateToLong(reader);
                    break;
                case "stamp_expire":
                    expiry = parseDateToLong(reader);
                    break;
                case "stamp_end":
                    end = parseDateToLong(reader);
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        return new MintSigningKeyDescriptor(
                publicSigningKey,
                mintMasterPublicKey,
                signature,
                start, expiry, end);
    }

    @Override
    public void close() {
        super.close();

        if (responseSigningKey != null) {
            responseSigningKey.close();
            responseSigningKey = null;
        }

        if (responseSignature != null) {
            responseSignature.close();
            responseSignature = null;
        }
    }

    /**
     * gets the mint's public master key
     *
     * @return the EdDSA master key
     */
    public EdDSAPublicKey getPublicMasterKey() {
        return publicMasterKey;
    }

    /**
     * gets the issue date of the master key
     *
     * @return the calendar pointing to the issue date
     */
    public Calendar getIssueDate() {
        return issueDate;
    }

    /**
     * gets the denomination descriptors from the result
     *
     * @return a list of denomination descriptors
     */
    public List<DenominationDescriptor> getDenominations() {
        return Collections.unmodifiableList(denominations);
    }

    /**
     * gets the signing keys from the result
     *
     * @return a list of signing key descriptors
     */
    public List<MintSigningKeyDescriptor> getSigningKeys() {
        return Collections.unmodifiableList(signingKeys);
    }

    @Override
    public boolean verify() {
        HashContext hashContext = new HashContext();

        for (DenominationDescriptor denomination : denominations) {
            try (Hash keyHash = denomination.getPublicKey().hash()) {
                hashContext.add(
                        keyHash.getHashStruct().getPointer(),
                        keyHash.getNativeSize());
            }
        }

        Hash finalHash = hashContext.finish();

        try (ECCSignaturePurpose purpose =
                     new ECCSignaturePurpose(
                             finalHash.getHashStruct().getPointer(),
                             finalHash.getNativeSize().intValue(),
                             TalerLibrary.Taler_Signatures.TALER_SIGNATURE_MINT_KEY_SET)) {

            boolean result = responseSignature.verify(purpose, responseSigningKey);

            responseSignature.close();

            return result;
        }
    }

    /**
     * gets the audited denominations from the result
     *
     * @return a list of auditor descriptors
     */
    public List<AuditorDescriptor> getAuditors() {
        return auditors;
    }
}
