/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet;

import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TALER_Amount;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary;

/**
 * Wrapper class for Taler denominations
 *
 * @author Oliver Broome
 */
public class TalerDenomination {
    private TALER_Amount amountStruct;


    public TalerDenomination(String talerEncoded) {
        amountStruct = new TALER_Amount.ByReference();
        TalerLibrary.TALER_string_to_amount(talerEncoded, amountStruct);
    }
}
