/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.merchant;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.FunctionalEndpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.InvalidSignatureException;
import org.gnunet.taler.wallet_android.database.model.objects.DepositPermission;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary;

import java.io.IOException;
import java.net.URL;

import static org.gnunet.taler.wallet_android.communication.CommunicationConstants.METHOD_POST;
import static org.gnunet.taler.wallet_android.communication.merchant.MerchantEndpointConstants.MERCHANT_PREFIX;

/**
 * Represents the /taler/pay endpoint of the mint API
 *
 * @author Oliver Broome
 */
public class PayEndpoint extends FunctionalEndpoint {
    /**
     * Creates a new Pay endpoint
     *
     * @param baseURL           the merchant's base URL
     * @param depositPermission the deposit permission to sign
     * @param coinPrivateKey    the private key of the coin to use
     * @throws IOException
     */
    public PayEndpoint(@NonNull URL baseURL, DepositPermission depositPermission, EdDSAPrivateKey coinPrivateKey) throws IOException {
        super(baseURL, MERCHANT_PREFIX + "/pay", METHOD_POST);

        try (ECCSignaturePurpose signaturePurpose = new ECCSignaturePurpose(
                depositPermission.getDepositStruct().getPointer(),
                depositPermission.getStructSize(),
                TalerLibrary.Taler_Signatures.TALER_SIGNATURE_WALLET_COIN_DEPOSIT);
             EdDSASignature signedDepositPermission = new EdDSASignature(signaturePurpose, coinPrivateKey)) {
            JsonData body = new JsonData(
                    signedDepositPermission.encode(),
                    coinPrivateKey.getPublicKey().encode());

            Gson gson = new Gson();

            prepareRequestBody(gson.toJson(body));
        }
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        //there is no response defined at this time, so this is left empty.
    }

    @Override
    protected void handleErrors(int responseCode, @NonNull JsonReader reader) throws IOException {
        if (responseCode == 401) {
            throw new InvalidSignatureException("reserve", reader);
        } else {
            handleDefaultError(responseCode, reader);
        }
    }

    @Override
    public boolean verify() {
        //there is nothing to verify at the moment.
        return true;
    }

    private class JsonData {
        private String dep_perm; //signed deposit permission
        private String eddsa_pub; //the key used for signing

        public JsonData(String dep_perm, String eddsa_pub) {
            this.dep_perm = dep_perm;
            this.eddsa_pub = eddsa_pub;
        }
    }
}
