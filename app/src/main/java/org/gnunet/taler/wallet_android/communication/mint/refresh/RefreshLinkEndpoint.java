/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.refresh;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.FunctionalEndpoint;

import java.io.IOException;
import java.net.URL;

import static org.gnunet.taler.wallet_android.communication.CommunicationConstants.METHOD_POST;
import static org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants.REFRESH_PREFIX;

/**
 * Represents the /refresh/link endpoint of the mint API
 *
 * @author Oliver Broome
 */
public class RefreshLinkEndpoint extends FunctionalEndpoint {
    /**
     * Initialises a new refresh/link endpoint
     *
     * @param baseURL the mint's base URL
     * @throws IOException
     */
    public RefreshLinkEndpoint(@NonNull URL baseURL) throws IOException {
        super(baseURL, REFRESH_PREFIX + "/link", METHOD_POST);
    }

    @Override
    public boolean verify() {
        return false;
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.close();
    }
}
