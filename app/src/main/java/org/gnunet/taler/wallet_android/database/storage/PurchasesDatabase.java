/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AUTOINCREMENT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BLOB;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BOOLEAN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.DEFAULT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.FALSE;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NOT_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.PRIMARY_KEY;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.TEXT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.UNIQUE;

/**
 * Allows access to the purchases table
 *
 * @author Oliver Broome
 */
public class PurchasesDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "purchases";

    public static final String COLUMN_URL = "url";
    public static final String COLUMN_TRANSACTION_ID = "tid";
    public static final String COLUMN_WIRE_HASH = "wire_hash";
    public static final String COLUMN_PRICE_VALUE = "price_value";
    public static final String COLUMN_PRICE_FRACTION = "price_fraction";
    public static final String COLUMN_PRICE_CURRENCY = "price_currency";
    public static final String COLUMN_CONTRACT_TEXT = "contract_text";
    public static final String COLUMN_CHECKOUT_DONE = "checkout_done";
    public static final String COLUMN_PURCHASE_OK = "purchase_ok";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + BEGIN_COLUMNS +
                    BaseColumns._ID + INTEGER + PRIMARY_KEY + AUTOINCREMENT + NEXT_COLUMN +
                    "-- product URL given to use by the merchant\n" +
                    COLUMN_URL + TEXT + NOT_NULL + NEXT_COLUMN +
                    COLUMN_TRANSACTION_ID + INTEGER + NOT_NULL + NEXT_COLUMN +
                    COLUMN_WIRE_HASH + BLOB + NOT_NULL + NEXT_COLUMN +
                    COLUMN_PRICE_VALUE + INTEGER + NOT_NULL + NEXT_COLUMN +
                    COLUMN_PRICE_FRACTION + INTEGER + NOT_NULL + NEXT_COLUMN +
                    COLUMN_PRICE_CURRENCY + BLOB + NOT_NULL + NEXT_COLUMN +
                    COLUMN_CONTRACT_TEXT + TEXT + NEXT_COLUMN +
                    "-- coins were allocated for the item\n" +
                    COLUMN_CHECKOUT_DONE + BOOLEAN + DEFAULT + FALSE + NEXT_COLUMN +
                    "-- we received a confirmation\n" +
                    COLUMN_PURCHASE_OK + BOOLEAN + DEFAULT + FALSE + NEXT_COLUMN +
                    UNIQUE + "(" + COLUMN_URL + ", " + COLUMN_TRANSACTION_ID + ")" +
                    END_COLUMNS;

    public PurchasesDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }
}
