/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import org.gnunet.taler.wallet_android.database.model.objects.CoinDescriptor;
import org.gnunet.taler.wallet_android.database.model.state.WithdrawalDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSASignature;
import org.gnunet.taler.wallet_android.utilities.databaseHelpers.Codec;

import java.sql.SQLWarning;

import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AND;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.AUTOINCREMENT;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BEGIN_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.BLOB;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.END_COLUMNS;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.INTEGER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.IS_PARAMETER;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NEXT_COLUMN;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.NOT_NULL;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.PRIMARY_KEY;
import static org.gnunet.taler.wallet_android.database.storage.DBConstants.REFERENCES;

/**
 * Allows access to the withdrawals table
 *
 * @author Oliver Broome
 */
public class WithdrawDatabase extends AbstractDatabase {
    public static final String TABLE_NAME = "withdraw";

    public static final String COLUMN_RESERVE_ID = "reserveid";
    public static final String COLUMN_DENOMINATION_ID = "denomid";
    public static final String COLUMN_RESERVE_SIGNATURE = "reserve_sig";
    public static final String COLUMN_BLIND_SESSION_PUB = "blind_session_pub";
    public static final String COLUMN_BLINDING_PRIV = "blinding_priv";
    public static final String COLUMN_COIN_PUB = "coin_pub";
    public static final String COLUMN_X_COIN_PRIV = "x_coin_priv";
    public static final String COLUMN_COIN_ENVELOPE = "coin_ev";

    public static final String TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + BEGIN_COLUMNS +
                    BaseColumns._ID + INTEGER + PRIMARY_KEY + AUTOINCREMENT + NEXT_COLUMN +
                    COLUMN_RESERVE_ID + INTEGER + REFERENCES + ReserveDatabase.TABLE_NAME + " (" + BaseColumns._ID + ")" + NEXT_COLUMN +
                    COLUMN_DENOMINATION_ID + INTEGER + REFERENCES + DenominationDatabase.TABLE_NAME + " (" + BaseColumns._ID + " )" + NEXT_COLUMN +
                    COLUMN_RESERVE_SIGNATURE + BLOB + NEXT_COLUMN +
                    COLUMN_BLIND_SESSION_PUB + BLOB + NEXT_COLUMN +
                    COLUMN_BLINDING_PRIV + BLOB + NEXT_COLUMN +
                    COLUMN_COIN_PUB + BLOB + NEXT_COLUMN +
                    COLUMN_X_COIN_PRIV + BLOB + NEXT_COLUMN +
                    COLUMN_COIN_ENVELOPE + BLOB +
                    END_COLUMNS;

    private static final String ID_WHERE = BaseColumns._ID + " = ?";
    private static final String PUBLIC_KEY_WHERE = COLUMN_COIN_PUB + " = ?";
    private static final String PLANNED_WHERE = COLUMN_COIN_ENVELOPE + " IS NULL " + AND + COLUMN_RESERVE_ID + IS_PARAMETER;
    private static final String PREPARED_WHERE = COLUMN_COIN_ENVELOPE + " IS" + NOT_NULL + AND + COLUMN_RESERVE_ID + IS_PARAMETER;

    public WithdrawDatabase(Context context, SQLiteOpenHelper databaseHelper) {
        super(context, databaseHelper);
    }

    public boolean addPlannedWithdrawals(long reserveId, CoinDescriptor[] coins, RSABlindingKey[] blindingKeys) {
        if (coins.length != blindingKeys.length) {
            throw new IllegalArgumentException("There needs to be an equal number of coins and blinding keys!");
        }

        boolean success = false;

        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            database.beginTransactionNonExclusive();

            int counter = 0;

            try {
                for (CoinDescriptor coin : coins) {
                    ContentValues values = new ContentValues(5);

                    values.put(COLUMN_RESERVE_ID, reserveId);
                    values.put(COLUMN_DENOMINATION_ID, coin.getDenomination().getId());
                    values.put(COLUMN_BLIND_SESSION_PUB, blindingKeys[counter].encode().toByteArray());
                    values.put(COLUMN_COIN_PUB, coin.getPublicKey().getStruct().q_y);
                    values.put(COLUMN_X_COIN_PRIV, coin.getPrivateKey().getStruct().d);

                    counter++;

                    if (database.insert(TABLE_NAME, null, values) == -1) {
                        throw new SQLWarning("A withdrawal entry could not be created!");
                    }
                }

                database.setTransactionSuccessful();
                success = true;
            } catch (SQLWarning warning) {
                success = false;
            } finally {
                database.endTransaction();
            }
        }

        return success;
    }

    public boolean addEnvelope(EdDSAPublicKey coinPublicKey, byte[] envelope) {
        return addEnvelope(coinPublicKey.getStruct().q_y, envelope);
    }

    public boolean addEnvelope(byte[] coinPubKeyBlob, byte[] envelope) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            ContentValues envelopeUpdate = new ContentValues(1);

            envelopeUpdate.put(COLUMN_COIN_ENVELOPE, envelope);

            return database.update(TABLE_NAME,
                    envelopeUpdate,
                    PUBLIC_KEY_WHERE,
                    new String[]{"x'" + Codec.bytesToHex(coinPubKeyBlob) + "'"}) == 1;
        }
    }

    public boolean addEnvelope(long coinId, byte[] envelope) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            ContentValues envelopeUpdate = new ContentValues(1);

            envelopeUpdate.put(COLUMN_COIN_ENVELOPE, envelope);

            return database.update(TABLE_NAME,
                    envelopeUpdate,
                    ID_WHERE,
                    new String[]{Long.toString(coinId)}) == 1;
        }
    }

    public boolean addSignature(long coinId, RSASignature signature) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            ContentValues signatureUpdate = new ContentValues(1);

            signatureUpdate.put(COLUMN_COIN_ENVELOPE, signature.encode());

            return database.update(TABLE_NAME,
                    signatureUpdate,
                    ID_WHERE,
                    new String[]{Long.toString(coinId)}) == 1;
        }
    }

    public boolean addSignature(byte[] coinPubKeyBlob, RSASignature signature) {
        try (SQLiteDatabase database = databaseHelper.getWritableDatabase()) {
            ContentValues envelopeUpdate = new ContentValues(1);

            envelopeUpdate.put(COLUMN_COIN_ENVELOPE, signature.encode());

            return database.update(TABLE_NAME,
                    envelopeUpdate,
                    PUBLIC_KEY_WHERE,
                    new String[]{"x'" + Codec.bytesToHex(coinPubKeyBlob) + "'"}) == 1;
        }
    }

    public Cursor getPlannedWithdrawalIds(long reserveId) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        return database.query(true, TABLE_NAME,
                new String[]{BaseColumns._ID},
                PLANNED_WHERE,
                new String[]{Long.toString(reserveId)},
                null, null, null, null);
    }

    public Cursor getPreparedWithdrawalIds(long reserveId) {
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        return database.query(true, TABLE_NAME,
                new String[]{BaseColumns._ID},
                PREPARED_WHERE,
                new String[]{Long.toString(reserveId)},
                null, null, null, null);
    }

    public Reader readerFor(Cursor cursor) {
        return new Reader(cursor);
    }

    private class Reader extends DatabaseReader {
        public Reader(Cursor cursor) {
            super(cursor);
        }

        @Override
        public WithdrawalDescriptor getCurrent() {
            return null;
        }
    }
}
