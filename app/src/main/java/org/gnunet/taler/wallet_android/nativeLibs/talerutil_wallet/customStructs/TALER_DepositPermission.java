/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.customStructs;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EddsaPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_HashCode;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TALER_Amount;
import org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TALER_CoinPublicInfo;

import java.util.Arrays;
import java.util.List;

/**
 * Custom structure for Taler deposit permissions
 *
 * @author Oliver Broome
 */
public class TALER_DepositPermission extends Structure {
    /**
     * public coin info
     */
    public TALER_CoinPublicInfo coinInfo;

    /**
     * the ID of the corresponding contract
     */
    public byte[] m = new byte[8]; //contract ID, definition unclear, will stick with uint64_t

    /**
     * the amount to be paid, in local byte order
     */
    public TALER_Amount amount;

    /**
     * hash of TALER_Contract->a
     */
    public GNUNET_HashCode a;

    /**
     * the hashed merchant wire information, as given in the contract
     */
    public GNUNET_HashCode h_wire;

    /**
     * the merchant's public key
     */
    public GNUNET_CRYPTO_EddsaPublicKey merch_pub;

    public TALER_DepositPermission() {
        super();
    }

    public TALER_DepositPermission(TALER_CoinPublicInfo coinInfo,
                                   byte[] contractId,
                                   TALER_Amount amount,
                                   Hash contractAFieldHash,
                                   GNUNET_HashCode hashedWireInfo,
                                   GNUNET_CRYPTO_EddsaPublicKey merchantPublicKey) {
        this.coinInfo = coinInfo;
        this.m = contractId;
        this.amount = amount;
        this.a = contractAFieldHash.getHashStruct();
        this.h_wire = hashedWireInfo;
        this.merch_pub = merchantPublicKey;
    }

    public TALER_DepositPermission(Pointer p) {
        super(p);
    }

    @Override
    protected List getFieldOrder() {
        return Arrays.asList("coinInfo", "m", "amount", "a", "h_wire", "merch_pub");
    }
}
