/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import org.gnunet.taler.wallet_android.R;
import org.gnunet.taler.wallet_android.database.DatabaseFactory;
import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.ReserveDescriptor;
import org.gnunet.taler.wallet_android.ui.fragments.ReserveGeneratorWorkerFragment;
import org.gnunet.taler.wallet_android.utilities.StableArrayAdapter;

import java.util.List;

/**
 * Activity for creating new reserves
 *
 * @author Oliver Broome
 */
public class ReserveGeneratorActivity extends AppCompatActivity implements ReserveGeneratorWorkerFragment.Callbacks {
    private long mintId;
    private String mintName;

    private Spinner denominationSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve_generator);

        mintId = getIntent().getLongExtra(IntentConstants.EXTRA_MINT_ID, -1);

        denominationSpinner = (Spinner) findViewById(R.id.denominationSpinner);

        List<String> currencyList = DatabaseFactory.getDenominationDatabase(getApplicationContext()).getCurrenciesForMint(mintId);

        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1,
                currencyList);

        denominationSpinner.setAdapter(adapter);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.asyncContainer, new ReserveGeneratorWorkerFragment());

        fragmentTransaction.commit();
    }

    public void launchReserveGeneratorResultActivity(View view) {
        generateReserve();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void generateReserve() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        ReserveGeneratorWorkerFragment asyncFragment =
                (ReserveGeneratorWorkerFragment) fragmentManager.findFragmentById(R.id.asyncContainer);

        AmountDescriptor amount = new AmountDescriptor(
                (String) denominationSpinner.getSelectedItem(),
                ((EditText) findViewById(R.id.cashAmount)).getText().toString());

        asyncFragment.generateKey(mintId, amount);
    }

    @Override
    public void onReservePersisted(ReserveDescriptor reserve) {
        Intent intent = new Intent(this, ReserveGeneratorResultsActivity.class);

        intent.putExtra(IntentConstants.EXTRA_RESERVE_PUBLIC_KEY, reserve.getPublicKey().encode());
        intent.putExtra(IntentConstants.EXTRA_RESERVE_CURRENCY, reserve.getInitialValue().getCurrency());
        intent.putExtra(IntentConstants.EXTRA_RESERVE_VALUE, reserve.getInitialValue().getValue());
        intent.putExtra(IntentConstants.EXTRA_RESERVE_FRACTION, reserve.getInitialValue().getFraction());

        startActivity(intent);
    }
}
