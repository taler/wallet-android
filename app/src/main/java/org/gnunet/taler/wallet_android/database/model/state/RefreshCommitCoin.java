/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.state;

import org.gnunet.taler.wallet_android.database.model.objects.CoinDescriptor;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Descriptor for the the RefreshCommitCoin table
 *
 * @author Oliver Broome
 */
public class RefreshCommitCoin {
    /**
     * The Refresh this RefreshCommitCoin is associated with
     */
    private Refresh refresh;

    /**
     * //TODO: find out what this is
     */
    private int cncIndex;

    /**
     * The new Coin that this RefreshCommitCoin should create
     */
    private CoinDescriptor newCoin;

    /**
     * The RefreshCommitCoin's private blinding key
     */
    private PrivateKey privateBlindingKey;

    /**
     * The RefreshCommitCoin's public blinding session key
     */
    private PublicKey publicBlindSessionKey;

    /**
     * //TODO: Find out what this is
     */
    private byte[] linkEnc;

    /**
     * The envelope for the new Coin
     */
    private byte[] coinEnvelope;

    public Refresh getRefresh() {
        return refresh;
    }

    public void setRefresh(Refresh refresh) {
        this.refresh = refresh;
    }

    public int getCncIndex() {
        return cncIndex;
    }

    public void setCncIndex(int cncIndex) {
        this.cncIndex = cncIndex;
    }

    public CoinDescriptor getNewCoin() {
        return newCoin;
    }

    public void setNewCoin(CoinDescriptor newCoin) {
        this.newCoin = newCoin;
    }

    public PrivateKey getPrivateBlindingKey() {
        return privateBlindingKey;
    }

    public void setPrivateBlindingKey(PrivateKey privateBlindingKey) {
        this.privateBlindingKey = privateBlindingKey;
    }

    public PublicKey getPublicBlindSessionKey() {
        return publicBlindSessionKey;
    }

    public void setPublicBlindSessionKey(PublicKey publicBlindSessionKey) {
        this.publicBlindSessionKey = publicBlindSessionKey;
    }

    public byte[] getLinkEnc() {
        return linkEnc;
    }

    public void setLinkEnc(byte[] linkEnc) {
        this.linkEnc = linkEnc;
    }

    public byte[] getCoinEnvelope() {
        return coinEnvelope;
    }

    public void setCoinEnvelope(byte[] coinEnvelope) {
        this.coinEnvelope = coinEnvelope;
    }
}
