/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.net.URL;

/**
 * Abstractions for functional endpoints that require verification, etc.
 *
 * @author Oliver Broome
 */
public abstract class FunctionalEndpoint extends Endpoint {
    /**
     * initialises the functional endpoint
     *
     * @param baseURL the mint's base URL
     * @param target  the endpoint to target
     * @param method  the HTTP method to use
     * @throws IOException
     */
    public FunctionalEndpoint(@NonNull URL baseURL, String target, @NonNull String method) throws IOException {
        super(baseURL, target, method);
    }

    /**
     * verifies the returned results of the endpoint
     *
     * @return true, if everything is all right
     */
    public abstract boolean verify();
}
