/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa;

import android.util.Log;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Memory;
import com.sun.jna.ptr.PointerByReference;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_HashCode;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Codec;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities.Hash;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

import static org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary.GNUNET_CRYPTO_rsa_PublicKey;
import static org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary.GNUNET_CRYPTO_rsa_private_key_get_public;
import static org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary.GNUNET_CRYPTO_rsa_public_key_cmp;
import static org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary.GNUNET_CRYPTO_rsa_public_key_decode;
import static org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary.GNUNET_CRYPTO_rsa_public_key_encode;
import static org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary.GNUNET_CRYPTO_rsa_public_key_free;

/**
 * Helper class for public RSA keys
 *
 * @author Oliver Broome
 */
public class RSAPublicKey implements AutoCloseable {
    private GNUNET_CRYPTO_rsa_PublicKey keyStruct;

    /**
     * Wraps an existing public key in memory
     *
     * @param keyStruct the Structure object to wrap
     */
    public RSAPublicKey(GNUNET_CRYPTO_rsa_PublicKey keyStruct) {
        this.keyStruct = keyStruct;
    }

    /**
     * Decodes a public key from an encoded representation, which can be a Base32-encoded
     *
     * @param gnunetEncoded the encoded String
     * @param fromJSON      set this to true if the String was retrieved from a JSON object and
     *                      should be converted to an S-Expression first
     */
    public RSAPublicKey(String gnunetEncoded, boolean fromJSON) {
        if (fromJSON) {
            //TODO: This could be improved by including the JSON functionality (json.c) of the mint
            Memory decodedSExpression = Codec.decode(gnunetEncoded);
            keyStruct = GNUNET_CRYPTO_rsa_public_key_decode(
                    decodedSExpression,
                    new NativeSize(decodedSExpression.size()));
            MemoryUtils.freePointer(decodedSExpression);
        } else {
            keyStruct = GNUNET_CRYPTO_rsa_public_key_decode(
                    gnunetEncoded,
                    new NativeSize(gnunetEncoded.getBytes().length));
        }
    }

    /**
     * Creates the private key corresponding to the given public key
     *
     * @param privateKey the private to derive the public key from
     */
    public RSAPublicKey(RSAPrivateKey privateKey) {
        keyStruct = GNUNET_CRYPTO_rsa_private_key_get_public(privateKey.getStruct());
    }

    /**
     * Encodes this public key as an S-Expression
     *
     * @return the encoded S-Expression
     */
    public String encode() {
        if (keyStruct != null) {
            PointerByReference ptrRef = new PointerByReference();
            NativeSize encodedSize = GNUNET_CRYPTO_rsa_public_key_encode(keyStruct, ptrRef);


            String result = ptrRef.getValue().getString(0);

            MemoryUtils.freePointerRef(ptrRef, true);

            return result;
        } else {
            Log.e("taler_rsa", "No key in memory!");
            return null;
        }
    }

    /**
     * Hashes this public key.
     *
     * @return the SHA512 hash of the underlying key structure
     */
    public Hash hash() {
        GNUNET_HashCode result = new GNUNET_HashCode.ByReference();
        GNUNetLibrary.GNUNET_CRYPTO_rsa_public_key_hash(keyStruct, result);

        return new Hash(result);
    }

    /**
     * Get the length of the public key in bits
     *
     * @return the key length in bits
     */
    public int getKeyBitLength() {
        return GNUNetLibrary.GNUNET_CRYPTO_rsa_public_key_len(keyStruct);
    }

    /**
     * Frees the resources associated with this public key
     */
    @Override
    public void close() {
        if (keyStruct != null) {
            GNUNET_CRYPTO_rsa_public_key_free(keyStruct);
            keyStruct = null;
        }
    }

    /**
     * Returns the wrapped key Structure object
     *
     * @return the wrapped GNUNET_CRYPTO_rsa_PublicKey Structure
     */
    public GNUNET_CRYPTO_rsa_PublicKey getStruct() {
        return keyStruct;
    }

    /**
     * Compares this public key with another
     *
     * @param otherPubKey the public key to compare this one with
     * @return true, if the two public keys are the same
     */
    public boolean compareTo(RSAPublicKey otherPubKey) {
        return GNUNET_CRYPTO_rsa_public_key_cmp(keyStruct, otherPubKey.getStruct()) == 0;
    }

}
