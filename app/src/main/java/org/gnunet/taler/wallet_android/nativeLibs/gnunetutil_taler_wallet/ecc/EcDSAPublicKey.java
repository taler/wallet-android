/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Pointer;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EcdsaPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Helper class for EcDSA public keys
 *
 * @author Oliver Broome
 */
public class EcDSAPublicKey implements AutoCloseable {
    private GNUNET_CRYPTO_EcdsaPublicKey keyStruct;

    /**
     * Generates the public key for the given EcDSA private key
     *
     * @param privateKey the public key to derive from
     */
    public EcDSAPublicKey(EcDSAPrivateKey privateKey) {
        keyStruct = new GNUNET_CRYPTO_EcdsaPublicKey();

        GNUNetLibrary.GNUNET_CRYPTO_ecdsa_key_get_public(privateKey.getStruct(), keyStruct);
    }

    /**
     * Creates a public key from a String encoded with GNUNET_STRINGS_data_to_string
     *
     * @param gnunetEncoded the encoded String
     */
    public EcDSAPublicKey(String gnunetEncoded) {
        keyStruct = new GNUNET_CRYPTO_EcdsaPublicKey();

        GNUNetLibrary.GNUNET_CRYPTO_ecdsa_public_key_from_string(
                gnunetEncoded,
                new NativeSize(gnunetEncoded.getBytes().length),
                keyStruct);
    }

    /**
     * Removes the private key from memory if it is present
     */
    @Override
    public void close() {
        if (keyStruct != null) {
            MemoryUtils.freeStruct(keyStruct);
            keyStruct = null;
        }
    }

    /**
     * Gets the underlying key Structure object
     *
     * @return the GNUNET_CRYPTO_EcdsaPublicKey Structure
     */
    public GNUNET_CRYPTO_EcdsaPublicKey getStruct() {
        return keyStruct;
    }

    /**
     * Gets a String representation of the public key
     *
     * @return a String created with GNUNET_STRINGS_data_to_string
     */
    public String encode() {
        Pointer stringPointer = GNUNetLibrary.
                GNUNET_CRYPTO_ecdsa_public_key_to_string(keyStruct);

        String result = stringPointer.getString(0);

        MemoryUtils.freePointer(stringPointer);

        return result;
    }
}
