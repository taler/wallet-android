/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.infrastructure;

import org.gnunet.taler.wallet_android.database.model.Descriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;

import java.util.Calendar;

/**
 * Descriptor for a mint signing key
 *
 * @author Oliver Broome
 */
public class MintSigningKeyDescriptor implements Descriptor, AutoCloseable {
    private EdDSAPublicKey signingKey;
    private EdDSAPublicKey masterPublicKey;
    private EdDSASignature signature;

    private Calendar start = Calendar.getInstance();
    private Calendar expiry = Calendar.getInstance();
    private Calendar end = Calendar.getInstance();

    /**
     * Creates a new mint signing key descriptor
     *
     * @param signingKey      the mint's signing key
     * @param masterPublicKey the mint's master public key
     * @param signature       the mint's signature over the key and expiry
     * @param start           when the key starts being valid
     * @param expiry          when the key expires
     * @param end             when all signatures made with this key are invalid
     */
    public MintSigningKeyDescriptor(EdDSAPublicKey signingKey, EdDSAPublicKey masterPublicKey, EdDSASignature signature,
                                    long start, long expiry, long end) {
        this.signingKey = signingKey;
        this.masterPublicKey = masterPublicKey;
        this.signature = signature;

        this.start.setTimeInMillis(start);
        this.expiry.setTimeInMillis(expiry);
        this.end.setTimeInMillis(end);
    }

    public MintSigningKeyDescriptor(EdDSAPublicKey publicSigningKey, Calendar expiry) {
        this.signingKey = publicSigningKey;
        this.expiry = expiry;
    }

    public EdDSAPublicKey getSigningKey() {
        return signingKey;
    }

    public void setSigningKey(EdDSAPublicKey publicSigningKey) {
        this.signingKey = publicSigningKey;
    }

    public Calendar getExpiry() {
        return expiry;
    }

    public void setExpiry(Calendar expiry) {
        this.expiry = expiry;
    }

    public Calendar getStart() {
        return start;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public Calendar getEnd() {
        return end;
    }

    public void setEnd(Calendar end) {
        this.end = end;
    }

    public EdDSAPublicKey getMasterPublicKey() {
        return masterPublicKey;
    }

    public void setMasterPublicKey(EdDSAPublicKey masterPublicKey) {
        this.masterPublicKey = masterPublicKey;
    }

    public EdDSASignature getSignature() {
        return signature;
    }

    public void setSignature(EdDSASignature signature) {
        this.signature = signature;
    }

    @Override
    public void close() {
        if (signingKey != null) {
            signingKey.close();
        }

        if (masterPublicKey != null) {
            masterPublicKey.close();
        }

        if (signature != null) {
            signature.close();
        }
    }
}
