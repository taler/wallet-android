/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

import java.nio.ByteBuffer;

/**
 * This interface wraps all functions that cannot be bound statically due to their use of varargs.
 * Blame JNA for not supporting them, I guess?
 *
 * @author Oliver Broome
 */
public interface GNUnetVarargLibrary extends Library {
    GNUnetVarargLibrary INSTANCE = (GNUnetVarargLibrary)
            Native.loadLibrary(GNUNetLibrary.JNA_LIBRARY_NAME,
                    GNUnetVarargLibrary.class);

    /**
     * @param key      authentication key<br>
     * @param rkey     root key<br>
     * @param salt     salt<br>
     * @param salt_len size of the salt<br>
     * @param argp     pair of void * & size_t for context chunks, terminated by NULL<br>
     *                 Original signature : {@code void GNUNET_CRYPTO_hmac_derive_key_v(GNUNET_CRYPTO_AuthKey*, GNUNET_CRYPTO_SymmetricSessionKey*, const void*, size_t, va_list)}
     * @ingroup hash<br>
     * @brief Derive an authentication key<br>
     */
    void GNUNET_CRYPTO_hmac_derive_key_v(GNUNET_CRYPTO_AuthKey key, GNUNET_CRYPTO_SymmetricSessionKey rkey, Pointer salt, NativeSize salt_len, Object... argp);

    /**
     * @param key      authentication key<br>
     * @param rkey     root key<br>
     * @param salt     salt<br>
     * @param salt_len size of the salt<br>
     * @param varArgs1 pair of void * & size_t for context chunks, terminated by NULL<br>
     *                 Original signature : {@code void GNUNET_CRYPTO_hmac_derive_key(GNUNET_CRYPTO_AuthKey*, GNUNET_CRYPTO_SymmetricSessionKey*, const void*, size_t, null)}
     * @ingroup hash<br>
     * @brief Derive an authentication key<br>
     */
    void GNUNET_CRYPTO_hmac_derive_key(GNUNET_CRYPTO_AuthKey key, GNUNET_CRYPTO_SymmetricSessionKey rkey, Pointer salt, NativeSize salt_len, Object... varArgs1);

    /**
     * @param result   buffer for the derived key, allocated by caller<br>
     * @param out_len  desired length of the derived key<br>
     * @param xtr_algo hash algorithm for the extraction phase, GCRY_MD_...<br>
     * @param prf_algo hash algorithm for the expansion phase, GCRY_MD_...<br>
     * @param xts      salt<br>
     * @param xts_len  length of @a xts<br>
     * @param skm      source key material<br>
     * @param skm_len  length of @a skm<br>
     * @param varArgs1 pair of void * & size_t for context chunks, terminated by NULL<br>
     * @return #GNUNET_YES on success<br>
     * Original signature : {@code int GNUNET_CRYPTO_hkdf(void*, size_t, int, int, const void*, size_t, const void*, size_t, null)}
     * @ingroup hash<br>
     * @brief Derive key<br>
     */
    int GNUNET_CRYPTO_hkdf(Pointer result, NativeSize out_len, int xtr_algo, int prf_algo, Pointer xts, NativeSize xts_len, Pointer skm, NativeSize skm_len, Object... varArgs1);

    /**
     * @param result   buffer for the derived key, allocated by caller<br>
     * @param out_len  desired length of the derived key<br>
     * @param xtr_algo hash algorithm for the extraction phase, GCRY_MD_...<br>
     * @param prf_algo hash algorithm for the expansion phase, GCRY_MD_...<br>
     * @param xts      salt<br>
     * @param xts_len  length of @a xts<br>
     * @param skm      source key material<br>
     * @param skm_len  length of @a skm<br>
     * @param argp     va_list of void * & size_t pairs for context chunks<br>
     * @return #GNUNET_YES on success<br>
     * Original signature : {@code int GNUNET_CRYPTO_hkdf_v(void*, size_t, int, int, const void*, size_t, const void*, size_t, va_list)}
     * @ingroup hash<br>
     * @brief Derive key<br>
     */
    int GNUNET_CRYPTO_hkdf_v(Pointer result, NativeSize out_len, int xtr_algo, int prf_algo, Pointer xts, NativeSize xts_len, Pointer skm, NativeSize skm_len, Object... argp);

    /**
     * @param result  buffer for the derived key, allocated by caller<br>
     * @param out_len desired length of the derived key<br>
     * @param xts     salt<br>
     * @param xts_len length of @a xts<br>
     * @param skm     source key material<br>
     * @param skm_len length of @a skm<br>
     * @param argp    va_list of void * & size_t pairs for context chunks<br>
     * @return #GNUNET_YES on success<br>
     * Original signature : {@code int GNUNET_CRYPTO_kdf_v(void*, size_t, const void*, size_t, const void*, size_t, va_list)}
     * @brief Derive key<br>
     */
    int GNUNET_CRYPTO_kdf_v(Pointer result, NativeSize out_len, Pointer xts, NativeSize xts_len, Pointer skm, NativeSize skm_len, Object... argp);

    /**
     * @param result   buffer for the derived key, allocated by caller<br>
     * @param out_len  desired length of the derived key<br>
     * @param xts      salt<br>
     * @param xts_len  length of @a xts<br>
     * @param skm      source key material<br>
     * @param skm_len  length of @a skm<br>
     * @param varArgs1 void * & size_t pairs for context chunks<br>
     * @return #GNUNET_YES on success<br>
     * Original signature : {@code int GNUNET_CRYPTO_kdf(void*, size_t, const void*, size_t, const void*, size_t, null)}
     * @ingroup hash<br>
     * @brief Derive key<br>
     */
    int GNUNET_CRYPTO_kdf(Pointer result, NativeSize out_len, Pointer xts, NativeSize xts_len, Pointer skm, NativeSize skm_len, Object... varArgs1);

    /**
     * Fill a buffer of the given size with count 0-terminated strings<br>
     * (given as varargs).  If "buffer" is NULL, only compute the amount<br>
     * of space required (sum of "strlen(arg)+1").<br>
     * Unlike using "snprintf" with "%s", this function will add<br>
     * 0-terminators after each string.  The<br>
     * "GNUNET_string_buffer_tokenize" function can be used to parse the<br>
     * buffer back into individual strings.<br>
     *
     * @param buffer   the buffer to fill with strings, can<br>
     *                 be NULL in which case only the necessary<br>
     *                 amount of space will be calculated<br>
     * @param size     number of bytes available in buffer<br>
     * @param count    number of strings that follow<br>
     * @param varArgs1 count 0-terminated strings to copy to buffer<br>
     * @return number of bytes written to the buffer<br>
     * (or number of bytes that would have been written)<br>
     * Original signature : {@code size_t GNUNET_STRINGS_buffer_fill(char*, size_t, unsigned int, null)}
     */
    NativeSize GNUNET_STRINGS_buffer_fill(ByteBuffer buffer, NativeSize size, int count, Object... varArgs1);

    /**
     * Given a buffer of a given size, find "count" 0-terminated strings<br>
     * in the buffer and assign the count (varargs) of type "const char**"<br>
     * to the locations of the respective strings in the buffer.<br>
     *
     * @param buffer   the buffer to parse<br>
     * @param size     size of the @a buffer<br>
     * @param count    number of strings to locate<br>
     * @param varArgs1 pointers to where to store the strings<br>
     * @return offset of the character after the last 0-termination<br>
     * in the buffer, or 0 on error.<br>
     * Original signature : {@code int GNUNET_STRINGS_buffer_tokenize(const char*, size_t, unsigned int, null)}
     */
    int GNUNET_STRINGS_buffer_tokenize(String buffer, NativeSize size, int count, Object... varArgs1);

    /**
     * @param kind     how serious is the error?<br>
     * @param message  what is the message (format string)<br>
     * @param varArgs1 arguments for format string<br>
     *                 Original signature : {@code void GNUNET_log_nocheck(GNUNET_ErrorType, const char*, null)}
     * @ingroup logging<br>
     * Main log function.<br>
     */
    void GNUNET_log_nocheck(int kind, String message, Object... varArgs1);

    /**
     * @param kind     how serious is the error?<br>
     * @param comp     component responsible for generating the message<br>
     * @param message  what is the message (format string)<br>
     * @param varArgs1 arguments for format string<br>
     *                 Original signature : {@code void GNUNET_log_from_nocheck(GNUNET_ErrorType, const char*, const char*, null)}
     * @ingroup logging<br>
     * Log function that specifies an alternative component.<br>
     * This function should be used by plugins.<br>
     */
    void GNUNET_log_from_nocheck(int kind, String comp, String message, Object... varArgs1);

    /**
     * @param iv       initialization vector<br>
     * @param skey     session key<br>
     * @param salt     salt for the derivation<br>
     * @param salt_len size of the @a salt<br>
     * @param varArgs1 pairs of void * & size_t for context chunks, terminated by NULL<br>
     *                 Original signature : {@code void GNUNET_CRYPTO_symmetric_derive_iv(GNUNET_CRYPTO_SymmetricInitializationVector*, GNUNET_CRYPTO_SymmetricSessionKey*, const void*, size_t, null)}
     * @ingroup crypto<br>
     * @brief Derive an IV<br>
     */
    void GNUNET_CRYPTO_symmetric_derive_iv(GNUNET_CRYPTO_SymmetricInitializationVector iv, GNUNET_CRYPTO_SymmetricSessionKey skey, Pointer salt, NativeSize salt_len, Object... varArgs1);

    /**
     * @param iv       initialization vector<br>
     * @param skey     session key<br>
     * @param salt     salt for the derivation<br>
     * @param salt_len size of the @a salt<br>
     * @param argp     pairs of void * & size_t for context chunks, terminated by NULL<br>
     *                 Original signature : {@code void GNUNET_CRYPTO_symmetric_derive_iv_v(GNUNET_CRYPTO_SymmetricInitializationVector*, GNUNET_CRYPTO_SymmetricSessionKey*, const void*, size_t, va_list)}
     * @brief Derive an IV<br>
     */
    void GNUNET_CRYPTO_symmetric_derive_iv_v(GNUNET_CRYPTO_SymmetricInitializationVector iv, GNUNET_CRYPTO_SymmetricSessionKey skey, Pointer salt, NativeSize salt_len, Object... argp);

}
