/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.exceptions;

import android.support.annotation.Nullable;

import com.google.gson.stream.JsonReader;

import java.io.IOException;

/**
 * Fallback exception for all errors not covered by the REST API documentation (or programming errors)
 *
 * @author Oliver Broome
 */
public class UnhandledErrorException extends IOException {
    public UnhandledErrorException(int errorResponse, @Nullable JsonReader reader) throws IOException {
        super(prepareMessage(errorResponse, reader));
    }

    private static String prepareMessage(int errorResponse, JsonReader reader) throws IOException {
        StringBuilder result = new StringBuilder(150);

        result.append("Unexpected error code (").append(errorResponse).append(") encountered!\n");

        if (reader != null) {
            result.append("\tEncountered JSON data:\n\t\t");
            result.append(reader.getPath());

            try {
                while (reader.hasNext()) {
                    result.append(reader.peek().toString()); //get the next token and append its type to the result
                    result.append("\n\t\t");
                    reader.skipValue(); //skip to the next top-level token
                }
            } catch (IOException e) {
                result.append("Parsing threw an IOException!");
                result.append(e.getMessage());
            }

            reader.close();
        }


        return result.toString();
    }
}
