/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.objects;

import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.database.model.Descriptor;

import java.io.Serializable;

/**
 * Descriptor class for Taler Amounts
 *
 * @author Oliver Broome
 */
public class AmountDescriptor implements Descriptor, Serializable {
    private int value;
    private int fraction;
    private String currency;

    /**
     * creates a new Amount descriptor
     *
     * @param currency the currency the amount should have
     * @param value    the amount's integer value
     * @param fraction the amount's fraction value
     */
    public AmountDescriptor(@NonNull String currency, int value, int fraction) {
        if (currency.length() != 3) {
            throw new IllegalArgumentException("Currency Strings must be exactly three characters!");
        }

        this.currency = currency;
        this.value = value;
        this.fraction = fraction;

        normalise();
    }

    /**
     * parses a value string
     *
     * @param currency the currency of the amount
     * @param value    the value represented as a string with a separator
     */
    public AmountDescriptor(@NonNull String currency, @NonNull String value) {
        this.currency = currency;

        int lastSeparator = value.lastIndexOf(".");

        if (lastSeparator < 0) {
            lastSeparator = value.lastIndexOf(",");
        }

        if (lastSeparator == 0) {
            throw new IllegalArgumentException("No valid separator was found!");
        }

        this.value = Integer.parseInt(value.substring(0, lastSeparator));

        String fractionString = value.substring(lastSeparator + 1, value.length());
        this.fraction = Integer.parseInt(fractionString);
        normalise();
    }

    /**
     * normalises the amount descriptor by converting fractions over and equal to 1000000 to value units
     */
    protected void normalise() {
        while (fraction >= 1000000) {
            fraction -= 1000000;
            value += 1;
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getFraction() {
        return fraction;
    }

    public void setFraction(int fraction) {
        this.fraction = fraction;
        normalise();
    }

    public String getCurrency() {
        return currency;
    }
}
