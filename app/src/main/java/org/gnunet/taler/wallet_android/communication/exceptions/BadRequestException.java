/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.exceptions;

import com.google.gson.stream.JsonReader;

import java.io.IOException;

/**
 * Exception for HTTP 400 errors
 *
 * @author Oliver Broome
 */
public class BadRequestException extends IOException {
    public BadRequestException(JsonReader reader) throws IOException {
        super(prepareMessage(reader));
    }

    private static String prepareMessage(JsonReader reader) throws IOException {
        StringBuilder result = new StringBuilder(100);
        String[][] outputs = new String[9][2];

        result.append("The mint reported a bad request!\n\t");

        reader.beginObject();

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "error":
                    outputs[0][0] = "Error message:";
                    outputs[0][1] = reader.nextString();
                    break;
                case "parameter":
                    outputs[1][0] = "Bogus parameter:";
                    outputs[1][1] = reader.nextString();
                    break;
                case "path":
                    outputs[2][0] = "Path to element:";
                    outputs[2][1] = reader.nextString();
                    break;
                case "offset":
                    outputs[3][0] = "Element offset:";
                    outputs[3][1] = reader.nextString();
                    break;
                case "index":
                    outputs[4][0] = "Element index:";
                    outputs[4][1] = reader.nextString();
                    break;
                case "object":
                    outputs[5][0] = "Object name:";
                    outputs[5][1] = reader.nextString();
                    break;
                case "currency":
                    outputs[6][0] = "Bad currency:";
                    outputs[6][1] = reader.nextString();
                    break;
                case "type_expected":
                    outputs[7][0] = "Expected type:";
                    outputs[7][1] = reader.nextString();
                    break;
                case "type_actual":
                    outputs[8][0] = "Actual type:";
                    outputs[8][1] = reader.nextString();
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        for (int i = 1; i < 9; i++) {
            result.append(outputs[i][0])
                    .append("\t")
                    .append(outputs[i][1])
                    .append("\n\t");
        }

        reader.close();

        result.append("\n");

        return result.toString();
    }
}
