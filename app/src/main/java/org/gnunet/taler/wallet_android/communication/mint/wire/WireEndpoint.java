/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication.mint.wire;

import android.support.annotation.NonNull;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.FunctionalEndpoint;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.gnunet.taler.wallet_android.communication.CommunicationConstants.METHOD_GET;
import static org.gnunet.taler.wallet_android.communication.mint.MintEndpointConstants.WIRE_PREFIX;
import static org.gnunet.taler.wallet_android.nativeLibs.talerutil_wallet.jna.TalerLibrary.Taler_Signatures.TALER_SIGNATURE_MINT_WIRE_TYPES;

/**
 * Represents the mint's /wire endpoint
 *
 * @author Oliver Broome
 */
public class WireEndpoint extends FunctionalEndpoint {
    private List<String> acceptedMethods;
    private EdDSASignature methodSignature;
    private EdDSAPublicKey signaturePublicKey;
    private ECCSignaturePurpose signaturePurpose;

    public WireEndpoint(@NonNull URL baseURL) throws IOException {
        super(baseURL, WIRE_PREFIX, METHOD_GET);
    }

    @Override
    public void close() {
        super.close();

        if (methodSignature != null) {
            methodSignature.close();
            methodSignature = null;
        }

        if (signaturePublicKey != null) {
            signaturePublicKey.close();
            signaturePublicKey = null;
        }

        if (signaturePurpose != null) {
            signaturePurpose.close();
            signaturePurpose = null;
        }
    }

    /**
     * returns all accepted wire transfer methods
     *
     * @return the list of methods (as Strings)
     */
    public List<String> getAcceptedMethods() {
        return acceptedMethods;
    }

    @Override
    public void parseResults(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "methods":
                    acceptedMethods = parseMethods(reader);
                    break;
                case "sig":
                    //because mints currently ony support one method, we can do this:
                    methodSignature = new EdDSASignature(null, reader.nextString());
                    break;
                case "pub":
                    signaturePublicKey = new EdDSAPublicKey(reader.nextString());
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        reader.close();

        signaturePurpose = new ECCSignaturePurpose(
                acceptedMethods.toArray(new String[acceptedMethods.size()]),
                true,
                TALER_SIGNATURE_MINT_WIRE_TYPES);
    }

    /**
     * parses the response for wire methods
     *
     * @param reader the reader pointing to the wire method array
     * @return the list of accepted methods
     * @throws IOException
     */
    private List<String> parseMethods(JsonReader reader) throws IOException {
        List<String> result = new ArrayList<>();

        reader.beginArray();

        while (reader.hasNext()) {
            result.add(reader.nextString());
        }

        reader.endArray();

        return result;
    }

    @Override
    public boolean verify() {
        boolean result = methodSignature.verify(signaturePurpose, signaturePublicKey);

        signaturePublicKey.close();
        signaturePurpose.close();
        methodSignature.close();

        return result;
    }
}
