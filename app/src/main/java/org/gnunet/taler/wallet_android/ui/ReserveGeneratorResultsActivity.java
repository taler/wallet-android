/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.gnunet.taler.wallet_android.R;
import org.gnunet.taler.wallet_android.database.model.objects.SepaDescriptor;

/**
 * Activity for displaying the results of reserve generation
 *
 * @author Oliver Broome
 */
public class ReserveGeneratorResultsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve_generator_results);

        Intent caller = getIntent();

        TextView transferInfo = (TextView) findViewById(R.id.transferInfo);
        transferInfo.setText(caller.getStringExtra(IntentConstants.EXTRA_RESERVE_PUBLIC_KEY));

        StringBuilder builder = new StringBuilder(7)
                .append(caller.getIntExtra(IntentConstants.EXTRA_RESERVE_VALUE, 0))
                .append(",")
                .append(caller.getIntExtra(IntentConstants.EXTRA_RESERVE_FRACTION, 0))
                .append(" ")
                .append(caller.getStringExtra(IntentConstants.EXTRA_RESERVE_CURRENCY));

        TextView amount = (TextView) findViewById(R.id.amount);
        amount.setText(builder.toString());


    }

    public void updateBankDetails(SepaDescriptor sepaDescriptor) {
        TextView iban = (TextView) findViewById(R.id.IBAN);
        TextView bic = (TextView) findViewById(R.id.BIC);
        TextView recipient = (TextView) findViewById(R.id.recipient);

        iban.setText(sepaDescriptor.getIban());
        bic.setText(sepaDescriptor.getBic());
        recipient.setText(sepaDescriptor.getRecipient());
    }
}
