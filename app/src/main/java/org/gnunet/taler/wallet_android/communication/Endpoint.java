/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.communication;

import android.Manifest;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.util.Log;

import com.google.gson.stream.JsonReader;

import org.gnunet.taler.wallet_android.communication.exceptions.BadRequestException;
import org.gnunet.taler.wallet_android.communication.exceptions.EndpointNotImplementedException;
import org.gnunet.taler.wallet_android.communication.exceptions.InternalErrorException;
import org.gnunet.taler.wallet_android.communication.exceptions.UnhandledErrorException;
import org.gnunet.taler.wallet_android.communication.exceptions.UnknownJsonFieldException;
import org.gnunet.taler.wallet_android.database.model.objects.AmountDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.DenominationDescriptor;
import org.gnunet.taler.wallet_android.database.model.objects.SepaDescriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.ECCSignaturePurpose;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSAPublicKey;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Common abstractions for the REST API endpoints
 *
 * @author Oliver Broome
 */
public abstract class Endpoint implements AutoCloseable {
    protected HttpURLConnection connection;

    /**
     * Creates a new Endpoint instance
     *
     * @param baseURL the base URL of the REST API
     * @param target  the path to the Endpoint, is appended to the base URL
     * @param method  the HTTP request method
     * @throws ProtocolException     thrown if the requested HTTP method could not be set.
     * @throws MalformedURLException thrown if we couldn't create a URL from the combined base URL
     *                               and target
     * @throws IOException           thrown if a connection could not be established
     */
    @RequiresPermission(Manifest.permission.INTERNET)
    public Endpoint(@NonNull URL baseURL, String target, @NonNull String method) throws IOException {
        URL targetUrl = new URL(baseURL, target);

        try {
            connection = (HttpURLConnection) targetUrl.openConnection();
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod(method);
        } catch (ProtocolException e) {
            Log.d("taler_connectivity", "Could not set HTTP method " + method);
            throw e;
        } catch (MalformedURLException e) {
            Log.e("taler_connectivity", "Could not open a connection to the given URL.", e);
            throw e;
        }
    }

    /**
     * Parses a Date string given by the mint
     *
     * @param reader the JsonReader pointing to the Date string
     * @return the number of seconds since the UNIX epoch
     * @throws IOException if something goes wrong with the reader
     */
    protected static long parseDateToLong(@NonNull JsonReader reader) throws IOException {
        String issued = reader.nextString();

        switch (issued) {
            case "\\/never\\/":
                return Long.MAX_VALUE;
            case "\\/forever\\/":
                return Long.MAX_VALUE;
            default:
                //      "\/Date(00000)\/"
                //      6------>_____<--2
                return Long.parseLong(
                        issued.substring(6, (issued.length() - 2)));
        }
    }

    /**
     * Creates a Calendar object for a specific point in time
     *
     * @param reader the JsonReader pointing to the Date string
     * @return the Calendar set to the date represented by the original string
     * @throws IOException if something goes wrong with the reader
     */
    protected static Calendar parseDateToCalendar(@NonNull JsonReader reader) throws IOException {
        Calendar result = Calendar.getInstance();

        result.setTimeInMillis(parseDateToLong(reader) * 1000); //we're getting the time in seconds, not milliseconds.

        return result;
    }

    /**
     * Parses a 'full' EdDSA signature
     *
     * @param reader the JsonReader pointing to the encoded signature
     * @return the decoded EdDSA signature
     * @throws IOException if something goes wrong with the reader
     */
    protected static EdDSASignature parseFullSignature(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        int purposeCode, size;
        String signatureString = null;
        ECCSignaturePurpose signaturePurpose = null;

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "purpose":
                    purposeCode = reader.nextInt();
                    break;
                case "size":
                    size = reader.nextInt();
                    break;
                case "eddsa_sig":
                    signatureString = reader.nextString();
                    break;
                case "eddsa_val":
                    signaturePurpose = new ECCSignaturePurpose(reader.nextString());
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        EdDSASignature signature = new EdDSASignature(signaturePurpose, signatureString);

        reader.endObject();

        return signature;
    }

    /**
     * Parses an AmountDescriptor from an Amount object from in a mint JSON response
     *
     * @param reader the JsonReader pointing to the encoded signature
     * @return the decoded EdDSA signature
     * @throws IOException if something goes wrong with the reader
     */
    protected static AmountDescriptor parseAmount(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        int value = 0,
                fraction = 0;
        String currency = null;

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "currency":
                    currency = reader.nextString();
                    break;
                case "value":
                    value = reader.nextInt();
                    break;
                case "fraction":
                    fraction = reader.nextInt();
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        return new AmountDescriptor(currency, value, fraction);
    }

    /**
     * Parses a list of DenominationDescriptors from an Denomination list object in a mint JSON response
     *
     * @param reader the JsonReader pointing to the encoded signature
     * @return a list of all Denominations offered by the mint
     * @throws IOException if something goes wrong with the reader
     */
    protected static List<DenominationDescriptor> parseDenominations(@NonNull JsonReader reader) throws IOException {
        reader.beginArray();

        List<DenominationDescriptor> result = new ArrayList<>();

        while (reader.hasNext()) {
            result.add(parseDenomination(reader));
        }

        reader.endArray();

        return result;
    }

    /**
     * Parses a single DenominationDescriptor from a Denomination array in a mint JSON response
     *
     * @param reader the JsonReader pointing to the encoded signature
     * @return a single, new, DenominationDescrpitor
     * @throws IOException if something goes wrong with the reader
     */
    protected static DenominationDescriptor parseDenomination(@NonNull JsonReader reader) throws IOException {
        reader.beginObject();

        RSAPublicKey publicKey = null;
        EdDSASignature masterSignature = null;
        long depositExpiry = 0L, legalExpiry = 0L, withdrawExpiry = 0L, start = 0L;
        AmountDescriptor depositFee = null, refreshFee = null, withdrawFee = null, value = null;

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "denom_pub":
                    publicKey = new RSAPublicKey(reader.nextString(), true);
                    break;
                case "fee_deposit":
                    depositFee = parseAmount(reader);
                    break;
                case "fee_refresh":
                    refreshFee = parseAmount(reader);
                    break;
                case "fee_withdraw":
                    withdrawFee = parseAmount(reader);
                    break;
                case "master_sig":
                    masterSignature = new EdDSASignature(null, reader.nextString());
                    break;
                case "stamp_expire_deposit":
                    depositExpiry = parseDateToLong(reader);
                    break;
                case "stamp_expire_legal":
                    legalExpiry = parseDateToLong(reader);
                    break;
                case "stamp_expire_withdraw":
                    withdrawExpiry = parseDateToLong(reader);
                    break;
                case "stamp_start":
                    start = parseDateToLong(reader);
                    break;
                case "value":
                    value = parseAmount(reader);
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        return new DenominationDescriptor(
                publicKey, masterSignature,
                value, depositFee, refreshFee, withdrawFee,
                start, depositExpiry, legalExpiry, withdrawExpiry
        );
    }

    /**
     * Parses a SEPA descriptor
     *
     * @param reader the JsonReader pointing to the encoded signature
     * @return a new SepaDescriptor
     * @throws IOException if something goes wrong with the reader
     */
    protected static SepaDescriptor parseSepaDetails(JsonReader reader) throws IOException {
        reader.beginObject();

        String recipient = null;
        String iban = null;
        String bic = null;
        EdDSASignature signature = null;

        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "receiver_name":
                    recipient = reader.nextString();
                    break;
                case "iban":
                    iban = reader.nextString();
                    break;
                case "bic":
                    bic = reader.nextString();
                    break;
                case "sig":
                    signature = new EdDSASignature(null, reader.nextString());
                    break;
                default:
                    throw new UnknownJsonFieldException(reader);
            }
        }

        reader.endObject();

        if (bic != null && iban != null && recipient != null) {
            return new SepaDescriptor(recipient, iban, bic, signature);
        } else {
            throw new RuntimeException("Could not create result!");
        }
    }

    /**
     * Sends a prepared request to the mint
     *
     * @param expectedResponseCode the response code we're expecting from the mint
     * @return true, if successful
     */
    protected boolean sendRequest(int expectedResponseCode) throws IOException {
        connection.connect();

        Log.d("taler_connectivity", "Connection established!");
        Log.d("taler_connectivity", connection.toString());

        int responseCode = connection.getResponseCode();

        try (JsonReader reader = getJsonReader()) {
            if (responseCode == expectedResponseCode) {
                parseResults(reader);
                return true;
            } else {
                Log.w("taler_connectivity", "Received an unexpected HTTP response code!");
                handleErrors(responseCode, reader);
                return false;
            }
        }

    }

    /**
     * Writes the String of JSON Data to the request body
     *
     * @param jsonData the entire JSON body as a String
     */
    protected void prepareRequestBody(@NonNull String jsonData) {
        try (OutputStream outStream = connection.getOutputStream()) {
            outStream.write(jsonData.getBytes());
            outStream.flush();
        } catch (IOException e) {
            Log.e("taler_connectivity", "Could not get the output stream for the connection!", e);
        }
    }

    /**
     * Returns a JsonReader on the current connection's InputStream, call after sendRequest()
     *
     * @return the JsonReader at the start of the JSON object
     * @throws IOException if something goes wrong with the reader
     */
    private JsonReader getJsonReader() throws IOException {
        InputStream in = connection.getInputStream();
        InputStreamReader inReader = new InputStreamReader(in, "UTF-8");

        return new JsonReader(inReader);
    }

    /**
     * Parses the results of a successful invocation of an Endpoint
     *
     * @param reader a JsonReader pointing to the beginning of the response JSON
     * @throws IOException if something goes wrong with the reader
     */
    protected abstract void parseResults(@NonNull JsonReader reader) throws IOException;

    /**
     * Handles the various errors an endpoint can produce by initiating an HTTP request
     *
     * @param responseCode the actual HTTP response code
     * @param reader       the JSON reader, pointing at president object in the JSON hierarchy
     * @throws IOException if something goes wrong with the reader
     */
    protected void handleErrors(int responseCode, @NonNull JsonReader reader) throws IOException {
        handleDefaultError(responseCode, reader);
    }

    /**
     * Fallback handler for all errors not handled by handleErrors, should be called at the end of that method
     *
     * @param responseCode error code returned by the API
     * @param reader       the JSONReader pointing to the JSON response body, if available.
     * @throws InternalErrorException  if the mint returned HTTP error 500
     * @throws BadRequestException     if the mint returned HTTP error 400
     * @throws UnhandledErrorException if the mint returns an error code not defined in the API
     * @throws IOException             if the JsonReader throws an exception of its own
     */
    protected void handleDefaultError(int responseCode, @Nullable JsonReader reader) throws IOException {
        if (reader != null) {
            switch (responseCode) {
                case 400:
                    throw new BadRequestException(reader);
                case 418:
                    reader.close();
                    Log.wtf("taler_communication", "The mint is a teapot. You may want to check your reality for glitches.");
                    break;
                case 500:
                    throw new InternalErrorException(reader);
                case 501:
                    reader.close();
                    throw new EndpointNotImplementedException();
                default:
                    throw new UnhandledErrorException(responseCode, reader);
            }

            reader.close();
        } else {
            throw new UnhandledErrorException(responseCode, null);
        }

    }

    /**
     * Connects to the endpoint URL and fetches or posts data
     */
    public void connect() throws IOException {
        sendRequest(200);
    }

    @Override
    public void close() {
        connection.disconnect();
    }
}
