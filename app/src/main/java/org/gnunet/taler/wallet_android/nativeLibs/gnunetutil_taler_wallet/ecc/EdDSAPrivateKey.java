/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Native;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_CRYPTO_EddsaPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;
import org.gnunet.taler.wallet_android.utilities.JNAhelpers.MemoryUtils;

/**
 * Helper class for EdDSA private keys
 *
 * @author Oliver Broome
 */
public class EdDSAPrivateKey implements AutoCloseable {
    private GNUNET_CRYPTO_EddsaPrivateKey keyStruct;
    private EdDSAPublicKey publicKey;

    /**
     * Creates a new EdDSA private key.
     */
    public EdDSAPrivateKey() {
        keyStruct = GNUNetLibrary.GNUNET_CRYPTO_eddsa_key_create();
    }

    /**
     * Creates an EdDSA private key from an encoded String
     *
     * @param gnunetEncoded the private key, encoded in the GNUnet format
     */
    public EdDSAPrivateKey(String gnunetEncoded) {
        int resultCode = GNUNetLibrary.GNUNET_CRYPTO_eddsa_private_key_from_string(
                gnunetEncoded,
                new NativeSize(Native.toByteArray(gnunetEncoded).length),
                keyStruct
        );

        if (resultCode != GNUNetLibrary.GNUNET_OK) {
            throw new RuntimeException("Could not successfully create key!");
        }
    }

    /**
     * Creates an EdDSA private key from a blob, taken, for example, from a database
     *
     * @param blob the byte array representing GNUNET_CRYPTO_EddsaPrivateKey.d
     */
    public EdDSAPrivateKey(byte[] blob) {
        this.keyStruct = new GNUNET_CRYPTO_EddsaPrivateKey(blob);
    }

    /**
     * Gets the key's structure.
     *
     * @return the key's GNUNET_CRYPTO_EddsaPrivateKey structure
     */
    public GNUNET_CRYPTO_EddsaPrivateKey getStruct() {
        return keyStruct;
    }

    public EdDSAPublicKey getPublicKey() {
        if (publicKey == null) {
            publicKey = new EdDSAPublicKey(this);
        }
        return publicKey;
    }

    public void setPublicKey(EdDSAPublicKey publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * Removes the private key from memory if it is present
     */
    @Override
    public void close() {
        if (keyStruct != null) {
            GNUNetLibrary.GNUNET_CRYPTO_eddsa_key_clear(keyStruct);
            MemoryUtils.freeStruct(keyStruct);
            keyStruct = null;
        }

        if (publicKey != null) {
            publicKey.close();
            publicKey = null;
        }

    }
}
