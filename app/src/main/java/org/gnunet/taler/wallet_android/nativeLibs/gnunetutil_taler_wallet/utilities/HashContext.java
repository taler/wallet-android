/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.utilities;

import com.ochafik.lang.jnaerator.runtime.NativeSize;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNET_HashCode;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.jna.GNUNetLibrary;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * Wrapper class for the GNUnet HashContext functions
 *
 * @author Oliver Broome
 */
public class HashContext implements AutoCloseable {
    private GNUNetLibrary.GNUNET_HashContext context;

    /**
     * Creates a new hashing context to which data can then be added
     */
    public HashContext() {
        context = GNUNetLibrary.GNUNET_CRYPTO_hash_context_start();
    }

    /**
     * Adds data to the hash context
     *
     * @param data Pointer to the data in memory
     * @param size length of the data
     */
    public void add(Pointer data, NativeSize size) {
        GNUNetLibrary.GNUNET_CRYPTO_hash_context_read(context, data, size);
    }

    /**
     * Adds data to the hash context
     *
     * @param data           the String to read into the hash context
     * @param nullTerminated whether the String should be null-terminated
     */
    public void add(String data, boolean nullTerminated) {
        ByteBuffer preparedBuffer = ByteBuffer.allocateDirect(data.length());
        byte[] toWrite;

        if (nullTerminated) {
            toWrite = Native.toByteArray(data, "ASCII");
            preparedBuffer.put(toWrite, 0, toWrite.length);
        } else {
            toWrite = data.getBytes(StandardCharsets.US_ASCII);
            preparedBuffer.put(toWrite, 0, toWrite.length);
        }

        add(Native.getDirectBufferPointer(preparedBuffer),
                new NativeSize(preparedBuffer.capacity()));
    }

    /**
     * Closes and frees the hash context and returns the final Hash
     *
     * @return the cumulative Hash of all data added to this context
     */
    public Hash finish() {
        GNUNET_HashCode result = new GNUNET_HashCode.ByReference();
        GNUNetLibrary.GNUNET_CRYPTO_hash_context_finish(context, result);

        context = null;
        return new Hash(result);
    }

    /**
     * Aborts the hashing operation and frees associated resources
     */
    @Override
    public void close() {
        if (context != null) {
            GNUNetLibrary.GNUNET_CRYPTO_hash_context_abort(context);
            context = null;
        }
    }
}
