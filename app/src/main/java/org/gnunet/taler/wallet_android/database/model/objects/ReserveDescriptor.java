/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.database.model.objects;

import android.support.annotation.NonNull;

import org.gnunet.taler.wallet_android.database.model.Descriptor;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPrivateKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSAPublicKey;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.ecc.EdDSASignature;
import org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.rsa.RSABlindingKey;

/**
 * Descriptor Class for reserves
 *
 * @author Oliver Broome
 */
public class ReserveDescriptor implements Descriptor, AutoCloseable {
    private long reserveID;
    private long mintId;

    private EdDSAPrivateKey privateKey;
    private EdDSAPublicKey publicKey;

    private AmountDescriptor initialValue;
    private AmountDescriptor currentValue;

    private RSABlindingKey publicBlindSessionKey;
    private EdDSASignature statusSignature;

    public ReserveDescriptor(long reserveID, long mintId,
                             EdDSAPrivateKey reservePrivateKey, @NonNull EdDSAPublicKey reservePublicKey,
                             AmountDescriptor initialValue, AmountDescriptor currentValue,
                             RSABlindingKey blindingKey, EdDSASignature statusSignature) {

        this.reserveID = reserveID;
        this.mintId = mintId;
        this.privateKey = reservePrivateKey;
        this.publicKey = reservePublicKey;
        this.initialValue = initialValue;
        this.currentValue = currentValue;
        this.publicBlindSessionKey = blindingKey;
        this.statusSignature = statusSignature;
    }

    public long getReserveID() {
        return reserveID;
    }

    public void setReserveID(long reserveID) {
        this.reserveID = reserveID;
    }

    public long getMintId() {
        return mintId;
    }

    public void setMintId(long mintId) {
        this.mintId = mintId;
    }

    public EdDSAPrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(EdDSAPrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public EdDSAPublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(EdDSAPublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public AmountDescriptor getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(AmountDescriptor initialValue) {
        this.initialValue = initialValue;
    }

    public AmountDescriptor getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(AmountDescriptor currentValue) {
        this.currentValue = currentValue;
    }

    public RSABlindingKey getPublicBlindSessionKey() {
        return publicBlindSessionKey;
    }

    public void setPublicBlindSessionKey(RSABlindingKey publicBlindSessionKey) {
        this.publicBlindSessionKey = publicBlindSessionKey;
    }

    public EdDSASignature getStatusSignature() {
        return statusSignature;
    }

    public void setStatusSignature(EdDSASignature statusSignature) {
        this.statusSignature = statusSignature;
    }

    @Override
    public void close() {
        if (privateKey != null) {
            privateKey.close();
            privateKey = null;
        }

        if (publicKey != null) {
            publicKey.close();
            publicKey = null;
        }

        if (publicBlindSessionKey != null) {
            publicBlindSessionKey.close();
            publicBlindSessionKey = null;
        }

        if (statusSignature != null) {
            statusSignature.close();
            statusSignature = null;
        }
    }
}
