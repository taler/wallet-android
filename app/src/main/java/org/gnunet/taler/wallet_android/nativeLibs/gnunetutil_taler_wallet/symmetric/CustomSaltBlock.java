/*
 * This file is part of TALER
 * Copyright (C) 2015 Christian Grothoff (and other contributing authors)
 *
 * TALER is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * TALER is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * TALER; see the file COPYING.  If not, If not, see <http://www.gnu.org/licenses/>
 */

package org.gnunet.taler.wallet_android.nativeLibs.gnunetutil_taler_wallet.symmetric;

import com.sun.jna.Memory;

import java.nio.charset.StandardCharsets;

/**
 * Wrapper for custom salts (mainly for use with testing functions)
 *
 * @author Oliver Broome
 */
public class CustomSaltBlock extends RandomBlock {
    /**
     * Converts a string (ASCII) to a block of memory for use as salt
     *
     * @param salt the String that should be prepared
     */
    public CustomSaltBlock(String salt) {
        byte[] stringBytes = salt.getBytes(StandardCharsets.US_ASCII);
        block = new Memory(stringBytes.length);

        block.write(0, stringBytes, 0, stringBytes.length);
    }
}
